Python prototype to model ice habit growth in a 0D simulation due to deposition. 
Based on the Work of Chen&Lamb (1994) as well as Jensen&Harrington (2015). '
Includes predefined postprocessing scripts that use reconstructed data from 
previous works.