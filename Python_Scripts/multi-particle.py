from plotpack import *
from glob import glob

#--------------------------------------------------------------------------------------------------
col = ['black','tab:blue','forestgreen']
# color_blind=['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6']
colors=['black','darkblue', 'tab:blue','lime', 'darkgreen','brown', 'orange', 'red',]
ntop      = 5000
# ytop      = ntop*0.05 
# r_start   = 1e-06
vt        = [3]
row       = 3
nmax      = 1

no        = 9

save_name = 'rime_mcsnow_multi_part_'+str(ntop)+'m_vt'+str(vt[0])
fname     ='./PDFs/'+save_name+'.pdf'
# directory = './mcsnow_output_riming/'
directory = '~/mcsnow/experiments/1d_habit_xi100000_nz500_lwc20_ncl0_dtc5_nrp200_rm10_rt0_vt3_h10-20_ba500_habit1/'

nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'mm', 'm_r', 'V_r']
#--------------------------------------------------------------------------------------------------
# df  = pd.read_csv(directory+'test_multi.dat', names=nam)
df  = pd.read_csv(directory+'mass2fr.dat', names=nam)
# df2 = pd.read_csv(directory+'test_multi_new.dat', names=nam)
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(20,32))
fig.subplots_adjust(wspace=0, hspace=0)
pp      = PdfPages(fname)
# fsize   = 18
font    = {'size'   : 34}
mp.rc('font', **font)
#--------------------------------------------------------------------------------------------------

# print(df.loc[df.mm == 2]['t'])

# ax4  = plt.subplot(row,nmax,4)
# ax1  = plt.subplot(row,nmax,1, sharex=ax4)
# ax2  = plt.subplot(row,nmax,2, sharex=ax4)
# ax3  = plt.subplot(row,nmax,3, sharex=ax4)

ax4  = plt.subplot(row,nmax,3)
ax1  = plt.subplot(row,nmax,1, sharex=ax4)
ax2  = plt.subplot(row,nmax,1, sharex=ax4)
ax3  = plt.subplot(row,nmax,2, sharex=ax4)

maxt=max(df['t'])

# ax1.set_xlabel('$t$ [min]')
ax1.set_ylabel('$z$ [m]')
ax1.set_xlim(0.,1. )
ax1.tick_params(axis='both', which='major', length=8, width=2)
ax1.tick_params(axis='both', which='minor', length=5, width=2)

# ax2.set_xlabel('$t$ [min]')
ax2.set_ylabel('$m$ [kg]')
ax2.set_yscale('log')
ax2.set_xlim(0., 1. )
ax2.tick_params(axis='both', which='major', length=8, width=2)
ax2.tick_params(axis='both', which='minor', length=5, width=2)

# ax3.set_xlabel('$t$ [min]')
ax3.set_ylabel('$\u03A6$')
ax3.set_yscale('log')
ax3.set_xlim(0.,1. )
ax3.set_ylim(min(df['phi']),max(df['phi']))
ax3.axhline(y=1, linestyle='--', color='gray') 
ax3.tick_params(axis='both', which='major', length=8, width=2)
ax3.tick_params(axis='both', which='minor', length=5, width=2)
ax3.set_yticklabels(['0','0','0.1','1','10'])

# ax4.set_xlabel('$t$ [min]')
ax4.set_xlabel('$\overline{t}$')
ax4.set_ylabel('$v_\mathrm{t}$ [m$\,\mathrm{s}^{-1}$]')
ax4.set_xlim(0.,1. )
ax4.set_ylim(0.,max(df['vt']) )
ax4.xaxis.set_minor_locator(MultipleLocator(.1))
ax4.yaxis.set_minor_locator(MultipleLocator(.1))
ax4.tick_params(axis='both', which='major', length=8, width=2)
ax4.tick_params(axis='both', which='minor', length=5, width=2)


plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)
plt.setp(ax3.get_xticklabels(), visible=False)

color=cm.jet(np.linspace(0,1,no))
for i,c in zip(range(1,no),colors):
    # ax1.plot(df.loc[df.mm == i,'t'],df.loc[df.mm == i, 'z'], color=c) #, label=lab[0])#
    ax2.plot(df.loc[df.mm == i,'t']/maxt,df.loc[df.mm == i, 'm_i'], color=c, label=str(i))#, 
      #marker='$'+str(i+1)+'$', markersize=10, markevery=10, markeredgecolor='black')
    ax3.plot(df.loc[df.mm == i,'t']/maxt,df.loc[df.mm == i, 'phi'], color=c)
    ax4.plot(df.loc[df.mm == i,'t']/maxt,df.loc[df.mm == i, 'vt'], color=c)
ax2.legend(fontsize=22, labelspacing=0.3, handletextpad=0.3)
#--------------------------------------------------------------------------------------------------

plt.tight_layout()

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
png = True
if png == True:
  plt.savefig('./PDFs/'+save_name+'.png', bbox_inches = 'tight', pad_inches = 0.1)

plt.show()
pp.close()
