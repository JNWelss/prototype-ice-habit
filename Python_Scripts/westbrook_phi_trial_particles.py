from plotpack import *
from glob import glob
import matplotlib.lines as mlines
import matplotlib.patheffects as pe

dir       = '~/DWD/Westbrook_Cylinder_Spheroids/'
dir2      = './input_files/'
file_name = 'Supplemental_material-Table_modified.xlsx'

xl_file = pd.ExcelFile(dir2+file_name)
file1   = dir + 'westbrook20_boehm89curve.dat'
file2   = dir + 'boehm/vt_boehm_westbrook_prolate_phi0.04_Ar1.0.dat'
file3   = dir + 'boehm/vt_boehm_westbrook_cylinder_phi5.00_Ar1.0.dat'
file4   = dir + 'boehm/vt_boehm_westbrook_prolate_phi1.00_Ar1.0.dat'
var     = ['i', 'm', 'phi', 'v', 'C_d', 'C_d2', 'Re', 'Re_d', 'X', 'q', 'Ar']

df      = pd.read_excel(xl_file, sheet_name=[0,1], header=2, names=["phi","X","Re","Re_d","C_d","A_r","Motion"])
string  = pd.read_excel(xl_file, sheet_name=[4,5], header=1, names=["ID","maxi","mini","phi","C_d","Re","A_r","Motion"])
tab1    = pd.read_csv(file1, delimiter = " ", names=['Re','C_d'])
tab2    = pd.read_csv(file2, delimiter = " ", names=var)
tab3    = pd.read_csv(file3, delimiter = " ", names=var)
tab4    = pd.read_csv(file4, delimiter = " ", names=var)

plates = df[0].dropna()
cols   = df[1].dropna()
# ros    = df[2].dropna()
# agg    = df[3].dropna()
st_di    = string[4].dropna()
st_pr    = string[5].dropna()

# alltypes = [plates,cols,ros,agg]

rc      = {"axes.spines.right" : True,
           "axes.spines.top" : True, }
plt.rcParams.update(rc)

msize  = 40
labels = ['B89','Plate-like','Column-like','Stringham D&O', 'Stringham C&P']
lab2   = ['0.04','5','5c']
colors = ['black','tab:blue','tab:red','black','black']
markers= ['o','H', '|','X','d']
#--------------------------------------------------------------------------------------------------
col, row = 1, 2
fig, ax = plt.subplots(col*row,figsize=(15*col,10*row))
pp  = PdfPages('./PDFs/westbrook_phi_data_Re_Cd_comparison_Boehm.pdf')
#--------------------------------------------------------------------------------------------------
ax1 = plt.subplot(row, col, 1)
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(1,10000)
ax1.set_ylim(0.2,25.)
ax1.set_xlabel('Re')
ax1.set_ylabel('$C_\mathrm{d}A_\mathrm{r}^{3/4}$')
m1=["$+$" if i=="S" else 'o' for i in plates['Motion']]
m2=["$+$" if i=="S" else 'o' for i in cols['Motion']]
# m3=["$+$" if i=="S" else 'o' for i in ros['Motion']]
# m4=["$+$" if i=="S" else 'o' for i in agg['Motion']]
cm = plt.cm.get_cmap('jet')
#--------------------------------------------------------------------------------------------------
# ax1.plot(tab1['Re'],tab1['C_d'], c=colors[0], linestyle=':')
normalize = mp.colors.LogNorm(vmin=.04, vmax=5)
outline   = pe.withStroke(linewidth=3.5, foreground='black')
#--------------------------------------------------------------------------------------------------
for x, y, z, phi in zip(plates["Re"],   plates["C_d"],  plates["A_r"],  plates["phi"]):
    sc = ax1.scatter(x, y*z**(3/4), alpha=0.7, c= phi, marker=markers[1], facecolors='none', s=msize, cmap=cm, norm=normalize)  
for x, y, z, phi in zip(cols["Re"],     cols["C_d"],    cols["A_r"],    cols['phi']):
    sc = ax1.scatter(x, y*z**(3/4), alpha=0.7, c= phi, marker=markers[2], facecolors='none', s=msize+1, cmap=cm, norm=normalize)
for x, y, z, phi in zip(st_di["Re"],    st_di["C_d"],   st_di["A_r"],   st_di['phi']):
    sc = ax1.scatter(x, y*z**(3/4), alpha=0.5, c= phi, marker=markers[3], facecolors='none', s=msize+1, cmap=cm, norm=normalize)
for x, y, z, phi in zip(st_pr["Re"],    st_pr["C_d"],   st_pr["A_r"],   st_pr['phi']):
    sc = ax1.scatter(x, y*z**(3/4), alpha=0.5, c= phi, marker=markers[4], facecolors='none', s=msize+1, cmap=cm, norm=normalize)   


line2,=ax1.plot(tab2['Re'],tab2['C_d']*tab2['q']**(3/4), c=colors[1], linestyle='--', path_effects=[outline], linewidth=2.5)
line3,=ax1.plot(tab3['Re'],tab3['C_d']*tab3['q']**(3/4), c=colors[2], linestyle='--', path_effects=[outline], linewidth=2.5)
# line4,=ax1.plot(tab4['Re'],tab4['C_d']*tab4['q']**(3/4), c=colors[0], linestyle='--')

cbar = plt.colorbar(sc, ax=ax1, cmap=cm, norm=normalize)
cbar.ax.set_title("$\phi$")
#--------------------------------------------------------------------------------------------------
ax1.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False,
                     bottom=True, top=False, left=True, right=False)
ax1.tick_params(axis='both', which='major', length=8, width=2, grid_color='r', grid_alpha=0.5)
ax1.tick_params(axis='both', which='minor', length=5, width=2, grid_color='r', grid_alpha=0.5)

ax1.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax1.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
handles = []

# handles.append(mlines.Line2D([], [], color=colors[0], marker='None', linestyle=':',
                    # markersize=10, label=labels[0]))
for u in range(1,5):
    handles.append(mlines.Line2D([], [], color=colors[u], marker=markers[u], linestyle='None',
                    markersize=10, label=labels[u]) )

leg = plt.legend(handles=handles)
ax1.add_artist(leg)
plt.legend(handles=[line2,line3], labels=lab2, loc='lower left', title='$\phi$')
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 2)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.2,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}$')
m1=["$+$" if i=="S" else 'o' for i in plates['Motion']]
m2=["$+$" if i=="S" else 'o' for i in cols['Motion']]
# m3=["$+$" if i=="S" else 'o' for i in ros['Motion']]
# m4=["$+$" if i=="S" else 'o' for i in agg['Motion']]
cm = plt.cm.get_cmap('jet')
#--------------------------------------------------------------------------------------------------
# ax1.plot(tab1['Re'],tab1['C_d'], c=colors[0], linestyle=':')
normalize = mp.colors.LogNorm(vmin=.04, vmax=5)
outline   = pe.withStroke(linewidth=3.5, foreground='black')
#--------------------------------------------------------------------------------------------------
for x, y, z, phi in zip(plates["Re"],   plates["C_d"],  plates["A_r"],  plates["phi"]):
    sc = ax2.scatter(x, y, alpha=0.7, c= phi, marker=markers[1], facecolors='none', s=msize, cmap=cm, norm=normalize)  
for x, y, z, phi in zip(cols["Re"],     cols["C_d"],    cols["A_r"],    cols['phi']):
    sc = ax2.scatter(x, y, alpha=0.7, c= phi, marker=markers[2], facecolors='none', s=msize+1, cmap=cm, norm=normalize)
for x, y, z, phi in zip(st_di["Re"],    st_di["C_d"],   st_di["A_r"],   st_di['phi']):
    sc = ax2.scatter(x, y, alpha=0.5, c= phi, marker=markers[3], facecolors='none', s=msize+1, cmap=cm, norm=normalize)
for x, y, z, phi in zip(st_pr["Re"],    st_pr["C_d"],   st_pr["A_r"],   st_pr['phi']):
    sc = ax2.scatter(x, y, alpha=0.5, c= phi, marker=markers[4], facecolors='none', s=msize+1, cmap=cm, norm=normalize)   


line2,=ax2.plot(tab2['Re'],tab2['C_d'], c=colors[1], linestyle='--', path_effects=[outline], linewidth=2.5)
line3,=ax2.plot(tab3['Re'],tab3['C_d'], c=colors[2], linestyle='--', path_effects=[outline], linewidth=2.5)
# line4,=ax1.plot(tab4['Re'],tab4['C_d']*tab4['q']**(3/4), c=colors[0], linestyle='--')

cbar = plt.colorbar(sc, ax=ax2, cmap=cm, norm=normalize)
cbar.ax.set_title("$\phi$")
#--------------------------------------------------------------------------------------------------
ax2.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False,
                     bottom=True, top=False, left=True, right=False)
ax2.tick_params(axis='both', which='major', length=8, width=2, grid_color='r', grid_alpha=0.5)
ax2.tick_params(axis='both', which='minor', length=5, width=2, grid_color='r', grid_alpha=0.5)

ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
leg = plt.legend(handles=handles)
ax2.add_artist(leg)
plt.legend(handles=[line2,line3], labels=lab2, loc='lower left', title='$\phi$')
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
# plt.show()