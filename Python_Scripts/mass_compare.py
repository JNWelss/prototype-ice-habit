from plotpack  import *
# from utilities import *
#--------------------------------------------------------------------------------------------------
directory = './'
names     = ['T','a','b','phi','m','V','vt']
fname     = 'T_range_-30-0_p1100.0_dt1.0_t_end600_r_start4e-06_mD_'
#--------------------------------------------------------------------------------------------------
taka_m   = pd.read_csv('./Takahashi91_m.dat', delimiter = ",", names=['T','m'])
# taka_phi = pd.read_csv('./paper_dat/Takahashi91_phi.dat', delimiter = ",", names=['T','phi'])
tab1     = pd.read_csv(directory+fname+'1.out', delimiter = ",", names=names)
tab2     = pd.read_csv(directory+fname+'2.out', delimiter = ",", names=names)
tab3     = pd.read_csv(directory+fname+'3.out', delimiter = ",", names=names)
#--------------------------------------------------------------------------------------------------
fig       = plt.figure(figsize=(22,15))
save_name = 'mass_compare'
fname     = 'PDFs/'+save_name+'.pdf'
pp        = PdfPages(save_name)

font    = {'size'   : 34}
mp.rc('font', **font)
#--------------------------------------------------------------------------------------------------
ax1  = plt.subplot(1,1,1)
# plt.tick_params(labelsize=22, width=2, length=7)

ax1.set_xlabel('T [K]')
ax1.set_ylabel('m [$\mu$g]')
# plt.title('crystal mass compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

ax1.plot(tab1["T"],tab1["m"]/1e-9, color='black',    label="habit prediction")
ax1.plot(tab2["T"],tab2["m"]/1e-9, color='darkgrey', label="m-D-relation")
ax1.plot(tab3["T"],tab3["m"]/1e-9, color='darkgreen',    label="spherical")
ax1.plot(taka_m["T"]+273.15,taka_m["m"], marker='s', markersize=8,linestyle='None', mfc='none', color='tab:blue', label='Takahashi 1991', markeredgewidth=2.5)
#--------------------------------------------------------------------------------------------------

ax1.set_yscale('log')
ax1.set_xlim(min(tab1["T"]),max(tab1["T"]))
ax1.set_ylim(1e-1,1e1)
ax1.xaxis.set_minor_locator(MultipleLocator(1))
ax1.tick_params(axis='both', which='major', length=8, width=2)
ax1.tick_params(axis='both', which='minor', length=5, width=2)
plt.legend(fontsize = 24)

#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
png = True
if png == True:
  plt.savefig('./PDFs/'+save_name+'.png', bbox_inches = 'tight', pad_inches = 0.1)

plt.show()
pp.close()