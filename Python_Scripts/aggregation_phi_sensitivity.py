from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'
var       = ['d1','d2','ce_boehm','ce_bulk','ce_spheres','rho_rime','vel_rel','St','N_Re',
             'N_ReBig','ce_BG','ce_Robin','ce_Slinn','ce_Holger']
vt        = [3]

dir_n   = ['1']
rime    = '000'

#--------------------------------------------------------------------------------------------------
# 0 = default
case    = 0
#---------------------
no      = [9]
maxx    = [150.]
maxy    = [1.0]
key     = ['']
entries = ['1601']
N_phi    = [.2,.4,.6,.8,1.,1.25,1.66,2.5,5.]
interv  = [20]
xlabel  = ['collected crystal radius $r_2$ [$\mu \mathrm{m}$]']
type_sp = ['aggregation']
#--------------------------------------------------------------------------------------------------
# Plot details
row, col = 1, 1
# c        = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 
#             'blue','red','midnightblue', 'slategrey']
c        = ['#7bccc4','#43a2ca','#0868ac', 'blue', 'black', 'orange','lightcoral','tomato', 'firebrick']
labels   = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)']

lines    = {'linewidth':'2.5'}
mp.rc('lines', **lines)
#--------------------------------------------------------------------------------------------------

# Read model data
for v in vt:
    # print(v)
    frames    = []
    os.system('csplit '+directory+'check_c2_v'+str(v)+       
        '/colleff_riming_Fr'+str(rime)+'_rhor600_'+type_sp[case]+'_sens_r050.dat \
                   '+entries[case]+' {'+str(no[case]-1)+'} -f split. >/dev/null')
    os.system('sed "s/^[ \t]*//" -i split.*')

    filenames = ['split.00']
    for n in range(1,no[case]):
        name = 'split.0'+str(n)
        os.system('sed -i 1d '+ name )
        filenames.append(name)

    frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenames]) )
#--------------------------------------------------------------------------------------------------
    result = pd.concat(frames, keys=dir_n)
    # exit()
#--------------------------------------------------------------------------------------------------
    fig, axes = plt.subplots(figsize=(20*col,20*row), nrows = 1, ncols = 1)#, sharey = True)
    fig.subplots_adjust(wspace=0.03, hspace=0)
    pp        = PdfPages('coll_efficiency_aggregation_sensitivity_v'+str(v)+'.pdf')

#--------------------------------------------------------------------------------------------------
    axes.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
    axes.frameon = False
#--------------------------------------------------------------------------------------------------
    # subplots
    for k in range(0,len(dir_n)):
        ax1 = fig.add_subplot(row,col,1)
        ax1.set_title('$r_1 = 50\,\mu \mathrm{m}$',        fontsize=fsize-2)
        ax1.set_xlabel(xlabel[case])
        ax1.set_ylabel('$E_\mathrm{c}$')
        ax1.set_xlim(0.,maxx[case])
        ax1.set_ylim(0.,maxy[case])
        ax1.set_xticks(np.arange(0, maxx[case]+10, interv[case]))

        # plt.setp(ax1.get_yticklabels(), visible=False)

        for i in range(no[case]):
            l1 = ax1.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_boehm'], 
                     color=c[i],label='$\phi$ = '+str(N_phi[i]) )

        leg1 = ax1.legend(fontsize = fsize-3)
#--------------------------------------------------------------------------------------------------
# additional legend
    # from matplotlib.lines import Line2D
    # if ref[case] == True:
    #     custom_lines = [Line2D([0], [0], color='black', lw=4, linestyle='solid'),
    #                     Line2D([0], [0], color='black', lw=4, linestyle='--'),
    #                     Line2D([0], [0], color='black', lw=4, linestyle='dotted')]
    # else:
    #     custom_lines = [Line2D([0], [0], color='black', lw=4, linestyle='solid'),
    #                     Line2D([0], [0], color='black', lw=4, linestyle='--')]

    # leg2 = ax1.legend(custom_lines, ['Model', 'B92', key[case]],loc=lo[case])
    # ax1.add_artist(leg1)
#--------------------------------------------------------------------------------------------------
    plt.tight_layout(rect=[0, 0.0, 1, 0.95])
    pp.savefig()
    # plt.show()
    pp.close()
    os.system('rm split*')
