from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '/home/jan/leonie_code/mcsnow/experiments/'
var       = ['d1','d2','ce_boehm','ce_bulk','ce_spheres','rho_rime','vel_rel','St','N_Re',
             'N_ReBig','ce_BG','ce_Robin','ce_Slinn','ce_Holger']
vt        = [3]

dir_n   = ['1']
rime    = '000'

#--------------------------------------------------------------------------------------------------
# 0 = oblate, 1 = cylinder, 2 = branched
case    = 1
#---------------------
fold    = ['oblate','cylinder','branched']
fname   = ['drop_spectrum','drop_spectrum','aggregation']
caption = ['drop radius $r$ [ym]','drop radius $r$ [ym]','collected radius $r$ [ym]']
no      = [10,8,8]
maxx    = [90.,140.,800.]
maxy    = [1.1,1.5,1.0]
# key     = ['Martin 81','Schlamp 75',' ']
key     = ['Martin 81','W&J 00',' ']
lo      = [2,5,5]
entries = ['181','280','1601']
ref     = [True,True,False]                 # additional reference available?
N_Re    = [([.1,.5,1.,2.,2.5,4.,5.,10.,20.,50.]), 
           ([.2,.5,.7,1.,2.,5.,10.,20.]), 
           ([.5,1.,2.,3.,5.,10.,20.,50.])]
interv  = [10,10,50]
#--------------------------------------------------------------------------------------------------
# Plot details
row, col = 1, 2
c        = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 
            'blue','midnightblue', 'slategrey']
labels   = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)']

lines    = {'linewidth':'2.5'}
mp.rc('lines', **lines)
#--------------------------------------------------------------------------------------------------
# Read paper data
df    = []
df2   = []
files = []
filx  = []
for i in range(1,no[case]+1):
    file = '~/DWD/Boehm_'+fold[case]+'/c'+str(i)+'.dat'
    files.append(file)
    if ref[case] == True:
        # fils = '~/DWD/Boehm_'+fold[case]+'/h'+str(i)+'.dat'
        fils = '~/DWD/WJ_'+fold[case]+'/c'+str(i)+'.dat'
        filx.append(fils)

df.append( pd.DataFrame([pd.read_csv(f, names=['r','Ec'], delimiter = ";") for f in files]) )
curves  = pd.concat(df, keys=dir_n)

if ref[case] == True:
    df2.append(pd.DataFrame([pd.read_csv(f, names=['r','Ec'], delimiter = ",") for f in filx]) )
    curves2 = pd.concat(df2, keys=dir_n)
# print(curves[0][0]['r'])
# exit()
#--------------------------------------------------------------------------------------------------
# Read model data
for v in vt:
    # print(v)
    frames    = []
    os.system('csplit '+directory+'check_c2_v'+str(v)+'_'+str(fold[case])+       
        '/colleff_riming_Fr'+str(rime)+'_rhor600_'+fname[case]+'_00'+str(case+1)+'.dat \
                   '+entries[case]+' {'+str(no[case]-1)+'} -f split. >/dev/null')
    os.system('sed "s/^[ \t]*//" -i split.*')

    filenames = ['split.00']
    for n in range(1,no[case]):
        name = 'split.0'+str(n)
        os.system('sed -i 1d '+ name )
        filenames.append(name)

    frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenames]) )
#--------------------------------------------------------------------------------------------------
    result = pd.concat(frames, keys=dir_n)
    # exit()
#--------------------------------------------------------------------------------------------------
    fig, axes = plt.subplots(figsize=(20*col,20*row), nrows = 1, ncols = 1)#, sharey = True)
    fig.subplots_adjust(wspace=0.03, hspace=0)
    pp        = PdfPages('./PDFs/coll_efficiency_Boehm_'+fold[case]+'_v'+str(v)+'.pdf')

#--------------------------------------------------------------------------------------------------
    axes.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
    axes.frameon = False
#--------------------------------------------------------------------------------------------------
    # subplots
    for k in range(0,len(dir_n)):
        ax1 = fig.add_subplot(row,col,1)
        # ax1.set_title('Boehm',        fontsize=fsize-2)
        ax1.set_xlabel(caption[case])
        ax1.set_xlim(0.,maxx[case])
        ax1.set_ylim(0.,maxy[case])
        ax1.set_xticks(np.arange(0, maxx[case]+10, interv[case]))

        ax2 = fig.add_subplot(row,col,2)

        # plt.setp(ax1.get_yticklabels(), visible=False)

        for i in range(no[case]):
            Ec = result.loc[dir_n[k]].loc[i][0]['ce_boehm']
            d1 = result.loc[dir_n[k]].loc[i][0]['d1']
            d2 = result.loc[dir_n[k]].loc[i][0]['d2']
            vel = result.loc[dir_n[k]].loc[i][0]['vel_rel']
            # Ec = np.ma.masked_array(Ec, mask=( 0.5 * d2 > d1 ))
            l1 = ax1.plot(d2*5e5, Ec, 
                     color=c[i],label=labels[i]+', $N_\mathrm{Re}=$'+str(N_Re[case][i]) )
            l2 = ax1.plot(curves[0][i]['r'], curves[0][i]['Ec'],  color=c[i], linestyle='--', label='_nolegend_')
            if ref[case] == True:
                l3 = ax1.plot(curves2[0][i]['r'], curves2[0][i]['Ec'],  color=c[i], linestyle='dotted', label='_nolegend_')
            ax2.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['vel_rel'], 
                     color=c[i])
            print(result.loc[dir_n[0]].loc[i][0]['N_ReBig'][0]/N_Re[case][i], result.loc[dir_n[0]].loc[i][0]['N_ReBig'][0] - N_Re[case][i])
        leg1 = ax1.legend(fontsize = fsize-3)
#--------------------------------------------------------------------------------------------------
# additional legend
    from matplotlib.lines import Line2D
    if ref[case] == True:
        custom_lines = [Line2D([0], [0], color='black', lw=4, linestyle='solid'),
                        Line2D([0], [0], color='black', lw=4, linestyle='--'),
                        Line2D([0], [0], color='black', lw=4, linestyle='dotted')]
    else:
        custom_lines = [Line2D([0], [0], color='black', lw=4, linestyle='solid'),
                        Line2D([0], [0], color='black', lw=4, linestyle='--')]

    leg2 = ax1.legend(custom_lines, ['Model', 'B92', key[case]],loc=lo[case])
    ax1.add_artist(leg1)
#--------------------------------------------------------------------------------------------------
    plt.tight_layout(rect=[0, 0.0, 1, 0.95])
    pp.savefig(bbox_inches = 'tight', pad_inches = 0)
    # plt.show()
    pp.close()
    os.system('rm split*')
