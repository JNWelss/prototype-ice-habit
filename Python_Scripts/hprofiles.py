from plotpack import *
from glob import glob
import sys
#--------------------------------------------------------------------------------------------------
skips       = [0,1,2,3]
nam         = ['z', 'Ns',
               'Nd(0)', 'Md(0)', 'Fn(0)', 'Fm(0)', 'Fmono(0)',
               'Nd(1)', 'Md(1)', 'Fn(1)', 'Fm(1)', 'Fmono(1)',
               'Nd(2)', 'Md(2)', 'Fn(2)', 'Fm(2)', 'Fmono(2)',
               'Nd(3)', 'Md(3)', 'Fn(3)', 'Fm(3)', 'Fmono(3)',
               'Nd(4)', 'Md(4)', 'Fn(4)', 'Fm(4)', 'Fmono(4)']
basedir     = '/experiments/'
file_format = '.dat'
pwd         = os.getcwd()
# print(pwd)
file        = pwd + basedir + sys.argv[1] + '.dat'
# file2       = pwd + basedir + sys.argv[2] + '.dat'
with open(file) as f:
    firstline = next(f)

n = int(firstline[6:9])+1
print(n)
#--------------------------------------------------------------------------------------------------
df_num = pd.read_csv(file, names=nam, sep='   ', engine='python') 
for i in range(4, 61):
  skips.append(skips[i-1] + 504)

df = pd.read_csv(file, names=nam, skiprows = skips, sep='   ', engine='python') 
list_df = [df[i:i+n] for i in range(0,df.shape[0],n)]
print(df.z)
print(df.info())
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(20,32))
fig.subplots_adjust(wspace=0, hspace=0)
# pp      = PdfPages(fname)
# fsize   = 18
font    = {'size'   : 14}
mp.rc('font', **font)


#--------------------------------------------------------------------------------------------------
ax1 = plt.subplot(1, 1, 1)
ax1.set_ylabel('$z$')
# ax1.set_xlabel('$\overline{ v_\mathrm{t} }$')
# ax1.set_xlim(0.1,10)
ax1.set_ylim(0,5000)
# ax1.set_xlim(1e-8,1e-5)
# ax1.set_xscale('log')
# ax1.set_xscale('log')
ax1.yaxis.set_ticks(np.arange(0, 5000, 100))
ax1.plot(list_df[10]['Nd(0)'],list_df[10]['z'], color='black')

# leg1 = ax1.legend(fontsize = fsize-3)

plt.show()