# L-W-relationships from Um et al
# Table 1
import numpy as np

# Check Temperature and call L-W-relationship. If outside T-Range, assign None
def lw_relation(L,W,Te):
    if ( (Te < -4.) and (Te > -6.) ) :
        W = av70_N1a(L)
    elif ( (Te < -6.) and (Te > -8.) ) :
        W = av70_N1e(L)
        #W = h74_1(L)
    elif ( (Te < -9.5) and (Te > -11.) ) or ( (Te < -18.5) and (Te > -20.) ):
        L = av70_C1g(W)
    elif ( (Te < -8.) and (Te > -10.) ) or (Te < -20): 
        W = av70_col(L)
    elif ( (Te < -10.) and (Te > -13.) ):
        L = av70_P1a(W)
    elif ( (Te < -12) and (Te > -17.) ):
        L = av70_P2e(W)
    else:
        L = None
        W = None
    return L, W

# C1e & C1f (-10 < T < -8; T < -20)
def av70_col(L):
  L = L * 1e6
  if ( L <= 200. ):
    W = -8.479 + 1.002 * L - 0.00234 * L**2
  else:
    W = 11.3 * L**(0.414)
  return W * 1e-6

#if ( (T < -10.) and (T > -13.) ):
def av70_P1a(W):
  W = W * 1e6
  if (W > 20.):
    return 2.020 * W**(0.449) * 1e-6

# if ( (T < -9.5) and (T > -11.) ) or ( (T < -18.5) and (T > -20.) ):
def av70_C1g(W):
  W = W * 1e6
  
  if ( ( W > 20. ) and (W < 500.) ):
      return 0.402 * W**(1.018) * 1e-6
  else: 
    return None

# -17 < T < -12
def h74_hexplate(W):
  W = W * 1e6

  if ( ( W > 80. ) and (W < 2000.) ):
    return (99.17 - 37.49 * np.log(W) + 3.844 * (np.log(W))**2 )* 1e-6
  else: 
    return None

# -4 < T < -6
def av70_N1a(L):
  L = L * 1e6
  return 1.099 * L**0.61078 * 1e-6

# -6 < T < -8
def av70_N1e(L):
  return (L)

# -4 < T < -6
def av70_P2e(D):
  D = D * 1e6
  return 2.020 * D**0.449 * 1e-6

# -20 < T < -50
def um07_bul(L):
  L = L * 1e6
  if ( (L >= 100) and (L <= 600)):
    return 7.14 * L**(0.455) * 1e-6
  else:
    return None

# -18 < T < -20
def hf72_bul(L):
  L = L * 1e6
  if (L <= 300):
    return 1.0993 * L**(0.7856)
  else:
    return 4.690 * L**(0.532)

# -8 < T < -10
def ma94_C1e(L):
  L = L * 1e6
  if (L < 100):
    W = 0.7 * L * 1e-6
  elif ((L >= 100) and (L < 1000)):
    W = 6.96 * L**(0.5) * 1e-6
  else:
    return None

def d74_4(L):
  if ( (L > 10) and (L < 1000)):
    return 0.5 * L**(0.930) * 1e-6
  else:
    return None

def h74_1(L):
  L = L * 1e6
  if ( (L > 100) and (L < 3200)):
    return (-0.6524 + 1.32 * np.log(L) - 0.0846 * np.log(L)**2)* 1e-6
  else:
    return None