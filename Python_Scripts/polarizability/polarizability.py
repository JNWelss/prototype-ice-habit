import numpy as np

def relative_permittivity_ice_air(app_ice_den):
    
    # Protat et al. 2007 JAMC 
    # Evaluation of Ice Water Content Retrievals from Cloud Radar Reflectivity 
    # and Temperature Using a Large Airborne In Situ Microphysical Database
    
    # app_ice_den must be in kg/m^3
    
    if np.any(app_ice_den > 917) or np.any(app_ice_den <= 0):
        print('Argument app_ice_den must be from 0 (not including) to 917 (including). The value is in km/m^3.')
        raise

    pi = 917    # density of pure ice [kg/m^3]
    ei = 3.168  # refractive index of pure ice, imaginary part neglected
    
    f  = app_ice_den / pi
    r  = (ei - 1) / (ei + 2)
    ef = (2 * f * r + 1) / (1 - f * r)
    
    return ef

def polarizability_ratio(axis_ratio: float, app_ice_den: float):
    
    # axis_ratio < 1 for oblate
    # axis_ratio > 1 for prolate
    
    try:
        l2 = np.zeros([axis_ratio.size])
    except:
        print('Argument axis_ratio must be of numpy.array type!')
        raise

    # sphere
    k     = np.where(axis_ratio == 1)
    l2 = 1/3

    #oblate
    k     = np.where(axis_ratio < 1)
    l2 = np.sqrt((1/axis_ratio)**2 - 1)
    l2 = (1 + l2**2) / l2**2 * (1 - 1 / (l2) * np.arctan(l2))

    #prolate
    k     = np.where(axis_ratio > 1)
    l2 = np.sqrt(1 - (1/axis_ratio)**2)
    l2 = (1 - l2**2) / l2**2 * (-1 + 1 / (2*l2) * np.log((1+l2)/(1-l2)))
    
    l1 = (1-l2) / 2

    try:
        
        er = relative_permittivity_ice_air(app_ice_den)    
        pr = ((er-1)*l1 + 1) / ((er-1)*l2 + 1)
        
    except:
        print('Please check that app_ice_den is a float variable, and not an e.g. array of list.')
        raise
    
    return pr