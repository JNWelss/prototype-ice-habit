from plotpack import *
from glob import glob
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm, LogNorm, LinearSegmentedColormap
import matplotlib.lines as mlines
#--------------------------------------------------------------------------------------------------
# markers = [".",",","o","v","^","<",">","1","2","3","4","8","s","p","P","*","h","H","+","x","X","D","d","|","_",0,1,2,3,4,5,6,7,8,9,10,11]
markers=['^','<','*']
ntop      = 4000

# ytop      = ntop*0.05 
# r_start   = 1e-06
vt        = [3]
row       = 1
nmax      = 1

no        = 8

save_name = './PDFs/poster_multi_part_'+str(ntop)+'m_vt'+str(vt[0])+'.pdf'
# directory = './mcsnow_output_riming/'
directory = '~/code_update/mcsnow/experiments/1d_habit_xi100000_nz100_lwc20_ncl0_dtc5_nrp200_rm10_rt0_vt3_h10-20_ba500_habit1/'

nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'mm', 'm_r', 'V_r', 'xi']
#--------------------------------------------------------------------------------------------------
df  = pd.read_csv(directory+'mass2fr.dat', names=nam, header=0)
# df2 = pd.read_csv(directory2+'mass2fr.dat', names=nam, header=0)
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(25*row,15*nmax))
fig.subplots_adjust(wspace=0, hspace=0)
pp      = PdfPages(save_name)

font    = {'size'   : 34}
mp.rc('font', **font)
#--------------------------------------------------------------------------------------------------
# colors = ["cornflowerblue","darkblue", "dimgrey",  "darkred", "firebrick"]
colors = ["darkred","red","darkorange", "darkgreen", "lightseagreen", "cornflowerblue", "darkblue"]
# cmap1 = ListedColormap(colors)
nodes = [0.0, 0.24, 0.48,0.5,0.52,0.76, 1.0]
cmap1 = LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))

#--------------------------------------------------------------------------------------------------
ax1  = plt.subplot(row,nmax,1)

ax1.set_xlabel('$\overline{t}$')
ax1.set_ylabel('$z$ [m]')
ax1.set_xlim(0.,1)#max(df['t']) )
ax1.set_ylim(min(df.z),ntop+50)
# ax1.set_xscale('log')
# ax1.set_yscale('log')
#--------------------------------------------------------------------------------------------------

# Create a continuous norm to map from data points to colors
# Use a boundary norm instead
# norm = plt.LogNorm(0.01, 5)
tmax = max(df.t)
# sizes= [25,50,100,200]
# markers=['.','p','D','H','*']
markers=['.','.','.','.','.']
sizes=[200,200,300,600,1000]
lim = [1e-12,1e-11,1e-10,1e-9,1e-8,1e-7]
lim2 = [1e-12,0.95,1.25,100]
#--------------------------------------------------------------------------------------------------
for i in range(0,no+1):

  phi = df.loc[df.xi == i, 'phi'].values
  m_i = df.loc[df.xi == i, 'm_i'].values
  t   = df.loc[df.xi == i,'t']/tmax
  z   = df.loc[df.xi == i,'z']

  # if i == 5:
    # print(phi)
  points = np.array([t, z]).T.reshape(-1, 1, 2)
  segments = np.concatenate([points[:-1], points[1:]], axis=1) 
  # Set the values used for colormapping
  lc = LineCollection(segments, cmap=cmap1, norm=LogNorm(0.1,10), zorder=-1)
  lc.set_array(phi)
  lc.set_linewidth(3.5)
  line = ax1.add_collection(lc)
  for idx, marker in enumerate(markers):  
    j = np.full(phi.shape, False)
    j = ( (m_i > lim[idx]) & (m_i < lim[idx+1]) ) 
    # if (i <= 1):
    #   markers[0] = '$'+str(i+1)+'$'
    #   markers[1] = ' '
    # # elif(i == 1):
    #   # markers[0] = ' '
    # else:
    markers[1] = '$'+str(i)+'$'
    ax1.scatter( t[j][:1], z[j][:1], color='black', marker=marker, s=sizes[idx], zorder=1)

#--------------------------------------------------------------------------------------------------
cbar = plt.colorbar(line, ticks=[.1, 1, 10])
cbar.ax.tick_params(labelsize=28)
cbar.ax.set_yticklabels(['0.1', '1', '10'])
cbar.ax.set_title("$\phi$")
#--------------------------------------------------------------------------------------------------

dot  = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=10, label='$> 10^{-12}\,$kg')
pent = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=15, label='$> 10^{-11}\,$kg')
diam = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=20, label='$> 10^{-10}\,$kg')
octa = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=25, label='$> 10^{-9}\,$kg')
star = mlines.Line2D([], [], color='black', marker='.', linestyle='None',
                          markersize=30, label='$> 10^{-8}\,$kg')

plt.legend(title="$m_\mathrm{i}$",handles=[dot,pent,diam,octa,star], fontsize=28, labelspacing=.3, handletextpad=0.1)
#--------------------------------------------------------------------------------------------------
ax1.yaxis.set_minor_locator(MultipleLocator(100))
ax1.tick_params(axis='both', which='major', length=8, width=2)
ax1.tick_params(axis='both', which='minor', length=5, width=2)
#--------------------------------------------------------------------------------------------------
def T_prof(x):
  return(-0.0062 * x)
#--------------------------------------------------------------------------------------------------

ax2 = ax1.twinx()
ax2.plot( df.z, T_prof(df.z) )
ax2.set_ylabel("T [°C]")
ax2.spines["left"].set_position(("axes", -0.12))
ax2.spines["left"].set_visible(True)
ax2.yaxis.set_label_position('left')
ax2.yaxis.set_ticks_position('left')
ax2.invert_yaxis()
ax2.yaxis.set_minor_locator(MultipleLocator(1))
ax2.tick_params(axis='both', which='major', length=8, width=2)
ax2.tick_params(axis='both', which='minor', length=5, width=2)
#--------------------------------------------------------------------------------------------------

plt.tight_layout(rect=[0, 0.0, 1, 0.95])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)

plt.show()
pp.close()
