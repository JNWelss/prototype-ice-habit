from plotpack import *
from glob import glob
import matplotlib.lines as mlines

dir       = '~/DWD/Westbrook_Cylinder_Spheroids/'
dir2      = './input_files/'
file_name = 'Supplemental_material-Table_modified.xlsx'

xl_file = pd.ExcelFile(dir2+file_name)
#file1   = dir + 'westbrook20_boehm89curve.dat'

filecyl = dir +  "boehm/vt_boehm_westbrook_cylinder_phi1.05_Ar1.0.dat"
filepro = dir +  "boehm/vt_boehm_westbrook_prolate_phi1.05_Ar1.0.dat"
#fileint = dir +  "boehm/vt_boehm_westbrook_cylinder_phi1.05_Ar1.0_inc0.3.dat"

var     = ['i', 'm', 'phi', 'v', 'C_d', 'C_d2', 'Re', 'Re_d', 'X', 'q', 'Ar']

df      = pd.read_excel(xl_file, sheet_name=[0,1,2,3], header=2, names=["phi","X","Re","Re_d","C_d","A_r","Motion"])
#tab1    = pd.read_csv(file1, delimiter = " ", names=['Re','C_d'])
tabcyl  = pd.read_csv(filecyl, delimiter = " ", names=var)
tabpro  = pd.read_csv(filepro, delimiter = " ", names=var)
#tabint  = pd.read_csv(fileint, delimiter = " ", names=var)
tabcyl2 = pd.read_csv(dir +  "boehm/vt_boehm_westbrook_cylinder_phi2.00_Ar1.0.dat", delimiter = " ", names=var)
tabcyl5 = pd.read_csv(dir +  "boehm/vt_boehm_westbrook_cylinder_phi5.00_Ar1.0.dat", delimiter = " ", names=var)
tabpro2 = pd.read_csv(dir +  "boehm/vt_boehm_westbrook_prolate_phi2.00_Ar1.0.dat", delimiter = " ", names=var)
tabpro5 = pd.read_csv(dir +  "boehm/vt_boehm_westbrook_prolate_phi5.00_Ar1.0.dat", delimiter = " ", names=var)

phi     = ["0.04", "0.10", "1.00", "2.00", "5.00" ]
Ar      = [0.2, 0.4, 0.6, 0.8, 1.0]
alls    = list()

for n in range(0,5):
    helps   = []
    for k in range(0,5):
        file = dir +  "boehm/vt_boehm_westbrook_prolate_phi"+phi[n]+"_Ar"+str(Ar[k])+".dat"
        helps.append(pd.read_csv(file, delimiter = " ", names=var))
    alls.append( helps )

# print(alls[4][4])

plates = df[0].dropna()
cols   = df[1].dropna()
ros    = df[2].dropna()
agg    = df[3].dropna()

alltypes = [plates,cols,ros,agg]

rc      = {"axes.spines.right" : True,
           "axes.spines.top" : True, }
plt.rcParams.update(rc)

msize  = 40
labels = ['B89','Plate-like','Column-like','Rosettes', 'Aggregates']
lab2   = ['0.1','1','5','5c']
colors = ['black','tab:red','tab:green','tab:blue','magenta']
ls     = ['--',':','-.',(0, (1, 10)),'solid']
cm     = plt.cm.get_cmap('jet')
#--------------------------------------------------------------------------------------------------
col, row = 1, 5
fig = plt.figure(figsize=(15*col,10*row))
pp  = PdfPages('./PDFs/westbrook_phicurves_Re_Cd_comparison_Boehm.pdf')
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 1)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.4,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}$')
markers=["$+$" if l=="S" else 'o' for l in alltypes[1]['Motion']]
ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
Re_West17  = [8.1362, 18.987, 175.37, 328.44]
Cd_West17  = [4.5274, 7.3484, 0.75039, 0.70656]
X_West17   = [299.71, 2649.3, 23079, 76218]
phi_West17 = [1.0476,1.0476,1.0476,1.0476]
for x, y, z, m in zip(alltypes[1]["Re"], alltypes[1]["C_d"], alltypes[1]["A_r"], markers):
    r,g,b,a = cm(z)
    sc = ax2.scatter(x, y, alpha=0.75, c=z, marker=m, facecolors='none', s=80, cmap=cm, vmin=0.1, vmax=0.9)

colors[0] = 'dimgrey'

cyl, = ax2.plot(tabcyl['Re'],tabcyl['C_d'], c=colors[2], linestyle=ls[0], label='') 
ax2.plot(tabcyl2['Re'],tabcyl2['C_d'], c=colors[2], linestyle=ls[1], label='') 
ax2.plot(tabcyl5['Re'],tabcyl5['C_d'], c=colors[2], linestyle=ls[2], label='') 
pro, = ax2.plot(tabpro['Re'],tabpro['C_d'], c=colors[0], linestyle=ls[0], label='1.0476')
ax2.plot(tabpro2['Re'],tabpro2['C_d'], c=colors[0], linestyle=ls[1], label='2.0')
ax2.plot(tabpro5['Re'],tabpro5['C_d'], c=colors[0], linestyle=ls[2], label='5.0')

ax2.scatter(Re_West17, Cd_West17, alpha=0.8, edgecolors='black' ,marker='s', facecolors='black', s=40)

ax2.tick_params(axis='both', which='major', length=8, width=2, grid_color='grey', grid_alpha=0.5)
ax2.tick_params(axis='both', which='minor', length=5, width=2, grid_color='grey', grid_alpha=0.5)
#--------------------------------------------------------------------------------------------------
cbar = plt.colorbar(sc, ax=ax2)
cbar.ax.set_title("$A_\mathrm{r}$")
#--------------------------------------------------------------------------------------------------
leg = plt.legend(loc='lower left', fontsize = 22, title='$\phi$', frameon=True)
legend2 = ax2.legend([cyl, pro], ["Cylinder", "Prolate"], loc="upper right", ncol=1, fontsize = 22, columnspacing = 1, handlelength=1.5, frameon=True)
for lh in legend2.legendHandles: 
            lh.set_linestyle('solid')
ax2.add_artist(leg)
#--------------------------------------------------------------------------------------------------

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 2)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.2,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}A_\mathrm{r}^{3/4}$')
markers=["$+$" if l=="S" else 'o' for l in alltypes[0]['Motion']]
ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
for x, y, z, m in zip(alltypes[0]["Re"], alltypes[0]["C_d"], alltypes[0]["A_r"], markers):
    r,g,b,a = cm(z)
    sc = ax2.scatter(x, y*z**(3/4), alpha=0.75, c=z, marker=m, facecolors='none', s=30, cmap=cm, vmin=0.1, vmax=0.9)

for i in range(0,5):
    r,g,b,a = cm(Ar[i])
    ax2.plot(alls[1][i]['Re'],alls[1][i]['C_d']*alls[1][i]['Ar']**(3./4.), c=(r,g,b,a), linestyle=ls[0], label=Ar[i]) 

#--------------------------------------------------------------------------------------------------
cbar = plt.colorbar(sc, ax=ax2)
cbar.ax.set_title("$A_\mathrm{r}$")
#--------------------------------------------------------------------------------------------------
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
textstr = '\n'.join((
        '$\phi = $'+phi[1],))

ax2.text(0.98, 0.98, textstr, transform=ax2.transAxes, fontsize=fsize-3,
            verticalalignment='top', horizontalalignment='right', bbox=props)
#--------------------------------------------------------------------------------------------------
leg = plt.legend(loc='lower left', title='$A_\mathrm{r}$')
ax2.add_artist(leg)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 3)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.2,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}A_\mathrm{r}^{3/4}$')
markers=["$+$" if l=="S" else 'o' for l in alltypes[3]['Motion']]
ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
for x, y, z, m in zip(alltypes[3]["Re"], alltypes[3]["C_d"], alltypes[3]["A_r"], markers):
    r,g,b,a = cm(z)
    sc = ax2.scatter(x, y*z**(3/4), alpha=0.75, c=z, marker=m, facecolors='none', s=30, cmap=cm, vmin=0.1, vmax=0.9)

for i in range(0,5):
    r,g,b,a = cm(Ar[i])
    ax2.plot(alls[2][i]['Re'],alls[2][i]['C_d']*alls[2][i]['Ar']**(3./4.), c=(r,g,b,a), linestyle=ls[0], label=Ar[i]) 

#--------------------------------------------------------------------------------------------------
cbar = plt.colorbar(sc, ax=ax2)
cbar.ax.set_title("$A_\mathrm{r}$")
#--------------------------------------------------------------------------------------------------
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
textstr = '\n'.join((
        '$\phi = $'+phi[2],))

ax2.text(0.98, 0.98, textstr, transform=ax2.transAxes, fontsize=fsize-3,
            verticalalignment='top', horizontalalignment='right', bbox=props)
#--------------------------------------------------------------------------------------------------
leg = plt.legend(loc='lower left', title='$A_\mathrm{r}$')
ax2.add_artist(leg)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 4)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.2,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}A_\mathrm{r}^{3/4}$')
markers=["$+$" if l=="S" else 'o' for l in alltypes[1]['Motion']]
ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
for x, y, z, m in zip(alltypes[1]["Re"], alltypes[1]["C_d"], alltypes[1]["A_r"], markers):
    r,g,b,a = cm(z)
    sc = ax2.scatter(x, y*z**(3/4), alpha=0.75, c=z, marker=m, facecolors='none', s=30, cmap=cm, vmin=0.1, vmax=0.9)

for i in range(0,5):
    r,g,b,a = cm(Ar[i])
    ax2.plot(alls[4][i]['Re'],alls[4][i]['C_d']*alls[4][i]['Ar']**(3./4.), c=(r,g,b,a), linestyle=ls[0], label=Ar[i]) 

#--------------------------------------------------------------------------------------------------
cbar = plt.colorbar(sc, ax=ax2)
cbar.ax.set_title("$A_\mathrm{r}$")
#--------------------------------------------------------------------------------------------------
props = dict(boxstyle='round', facecolor='white', alpha=0.5)
textstr = '\n'.join((
        '$\phi = $'+phi[4],))

ax2.text(0.98, 0.98, textstr, transform=ax2.transAxes, fontsize=fsize-3,
            verticalalignment='top', horizontalalignment='right', bbox=props)
#--------------------------------------------------------------------------------------------------
leg = plt.legend(loc='lower left', title='$A_\mathrm{r}$')
ax2.add_artist(leg)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 5)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1,10000)
ax2.set_ylim(0.4,25.)
ax2.set_xlabel('Re')
ax2.set_ylabel('$C_\mathrm{d}$')
markers=["$+$" if l=="S" else 'o' for l in alltypes[1]['Motion']]
ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
for x, y, z, m in zip(alltypes[1]["Re"], alltypes[1]["C_d"], alltypes[1]["A_r"], markers):
    r,g,b,a = cm(z)
    sc = ax2.scatter(x, y, alpha=0.75, c=z, marker=m, facecolors='none', s=30, cmap=cm, vmin=0.1, vmax=0.9)

for i in range(3,5):
    ax2.plot(alls[i][4]['Re'],alls[i][4]['C_d'], c=colors[2], linestyle=ls[i-3], label=phi[i]) 

ax2.plot(tabcyl2['Re'],tabcyl2['C_d'], c=colors[2], linestyle=ls[2], label='2.00 c') 
ax2.plot(tabcyl5['Re'],tabcyl5['C_d'], c=colors[2], linestyle=ls[4], label='5.00 c') 

cbar = plt.colorbar(sc, ax=ax2)
cbar.ax.set_title("$A_\mathrm{r}$")
leg = plt.legend(loc='lower left', title='$\phi$')
ax2.add_artist(leg)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------
pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
# plt.show()

print('Fin')