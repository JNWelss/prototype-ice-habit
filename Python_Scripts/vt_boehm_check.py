from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'
var       = ['d','m','phi','vt','vt_hw','vt_kc']
vt        = 3

row       = 3
col       = 1
#--------------------------------------------------------------------------------------------------
file1    = directory+'vt_boehm_vary_phi_cylinder.dat'
file2    = directory+'vt_boehm_vary_phi_prolate.dat'
file3    = directory+'vt_boehm_vary_d_cylinder.dat'
file4    = directory+'vt_boehm_vary_d_prolate.dat'
file5    = directory+'vt_boehm_vary_d_oblate.dat'
tab1     = pd.read_csv(file1, delimiter = " ", names=var)
tab2     = pd.read_csv(file2, delimiter = " ", names=var)
tab3     = pd.read_csv(file3, delimiter = " ", names=var)
tab4     = pd.read_csv(file4, delimiter = " ", names=var)
tab5     = pd.read_csv(file5, delimiter = " ", names=var)
#--------------------------------------------------------------------------------------------------
dir       = '~/Documents/Westbrook_Cylinder_Spheroids/'
file_name = 'Supplemental_material-Table_modified.xlsx'
xl_file = pd.ExcelFile(dir+file_name)
df      = pd.read_excel(xl_file, sheet_name=[0,1], header=2, names=["phi","X","Re","Re_d","C_d","A_r","Motion"])
# string  = pd.read_excel(xl_file, sheet_name=[4,5], header=1, names=["ID","maxi","mini","phi","C_d","Re","A_r","Motion"])
plates = df[0].dropna()
cols   = df[1].dropna()
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(15*col,10*row))
pp  = PdfPages('./PDFs/vt_boehm_check.pdf')
#--------------------------------------------------------------------------------------------------
# vterm_bohm = N_Re * atmo%eta / (d * atmo%rho)
#   C_D = pi / 2. * d / v**2 * ATM
#   ATM = (rho_i - rho_a) * grav / rho_a
#     d = v**2 * C_D * 2/pi * ATM
#     d = Re * eta / (vt * rho_a)
#    vt = Re * eta / (d * rho_a)

#    vt = ((Re * eta * pi * ATM) / C_D * rho_a)^(1/3)
 
rho_a = 1.045               # @ z = 2000m 
eta   = 1.653e-5            # @ z = 2000m
rho_i = 919. 
grav  = 9.81

ATM   = (rho_i - rho_a) * grav / rho_a
denom = 3. * cols["C_d"] * rho_a
vt_c  = ( cols["Re"] * eta * ATM / denom )**(1/3)  
denom = 3. * plates["C_d"] * rho_a
vt_p  = ( plates["Re"] * eta * ATM * plates["phi"] / denom )**(1/3)

a_c = 0.5 * cols["Re"] * eta / (vt_c * rho_a) 
r_c = a_c * cols["phi"]
r_equi_c = np.sqrt( a_c * r_c * cols["A_r"] ) 
# print(a_c, r_equi_c)
vt_equi_c = cols["Re"] * eta / (2. * r_equi_c * rho_a)

# print(vt_c / vt_equi_c)
# exit()

#--------------------------------------------------------------------------------------------------
ax1 = plt.subplot(row, col, 1)
ax1.set_xlabel('$\phi$')
# ax1.set_ylabel('$v_\mathrm{t} \,[\mathrm{m\,s}^{-1}]$')
ax1.set_ylabel('$\overline{ v_\mathrm{t} }$')
ax1.set_xlim(0.1,10)
ax1.set_ylim(0.5,1.01)
ax1.set_xscale('log')
# ax1.set_yscale('log')
vtmax=max(np.amax(tab1["vt"]),np.amax(tab2["vt"]))
ax1.plot(tab1["phi"],tab1["vt"]/vtmax, color='black', label='cylinder')
ax1.plot(tab2["phi"],tab2["vt"]/vtmax, color='red', label='prolate')
ax1.scatter(cols["phi"], vt_c/vt_equi_c, alpha=0.8, edgecolors="blue",marker='o', facecolors='none', s=20)

leg1 = ax1.legend(fontsize = fsize-3)
ax1.axvline(x=1.)
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 2)
ax2.set_xlabel('d [$\mu \mathrm{m}$]')
ax2.set_ylabel('$v_\mathrm{t} \,[\mathrm{m\,s}^{-1}]$')
# ax2.set_xlim(0.,np.max(tab3["d"]))
# ax2.set_ylim(0.,max(np.max(tab3["vt"]), np.max(tab4["vt"])))
ax2.set_xscale('log')
ax2.set_yscale('log')

ax2.plot(tab3["d"],tab3["vt"], color='black', label='cylinder')
ax2.plot(tab4["d"],tab4["vt"], color='red', label='prolate')
ax2.plot(tab3["d"],tab3["vt_hw"], color='green', label='HW10')
ax2.plot(tab4["d"],tab4["vt_kc"], color='blue', label='KC05')

leg2 = ax2.legend(fontsize = fsize-3)
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
textstr = '\n'.join((
    r'$\phi=1.2$',)
    )

ax2.text(0.02, 0.75, textstr, transform=ax2.transAxes, fontsize=fsize-3,
        verticalalignment='top', bbox=props)

ax1.yaxis.set_minor_locator(MultipleLocator(.05))
ax1.tick_params(axis='both', which='major', length=8, width=2)
ax1.tick_params(axis='both', which='minor', length=5, width=2)
#--------------------------------------------------------------------------------------------------
ax3 = plt.subplot(row, col, 3)
ax3.set_xlabel('d [$\mu \mathrm{m}$]')
ax3.set_ylabel('$v_\mathrm{t} \,[\mathrm{m\,s}^{-1}]$')
# ax3.set_xlim(0.,np.max(tab3["d"]))
# ax3.set_ylim(0.,max(np.max(tab3["vt"]), np.max(tab4["vt"])))
ax3.set_xscale('log')
ax3.set_yscale('log')

ax3.plot(tab5["d"],tab5["vt"], color='red', label='oblate')
ax3.plot(tab5["d"],tab5["vt_hw"], color='green', label='HW10')
ax3.plot(tab5["d"],tab5["vt_kc"], color='blue', label='KC05')

leg3 = ax3.legend(fontsize = fsize-3)
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
textstr = '\n'.join((
    r'$\phi=0.8$',)
    )

ax3.text(0.02, 0.8, textstr, transform=ax3.transAxes, fontsize=fsize-3,
        verticalalignment='top', bbox=props)
#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)

pp.close()
plt.show()