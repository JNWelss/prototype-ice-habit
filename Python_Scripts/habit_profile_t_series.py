from plotpack  import *
from utilities import *
#--------------------------------------------------------------------------------------------------
col = ['black','tab:blue','forestgreen']
lab = ['no habit','habit', 'spherical'] 
# lab = ['no habit','habit', 'IGF=1 sublim.'] 

ntop      = 5000
ytop      = ntop*0.05 
r_start   = 1e-06
vt        = [3,3,3]
row       = 2
nmax      = 3

save_name = './mcsnow_plots/mcsnow_habit_profile_'+str(r_start)+'_'+str(ntop)+'m_vt'+str(vt[1])+'.pdf'
directory = './mcsnow_output/'
nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff']
nam2      = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'IGF_vent']
#--------------------------------------------------------------------------------------------------
tab  = pd.read_csv(directory+'mcsnow_ideal_nohabit_prof_'+str(r_start)+'_'+str(ntop)+'m_vt'+str(vt[0])+'.dat', names=nam)
tab2 = pd.read_csv(directory+'mcsnow_ideal_prof_'        +str(r_start)+'_'+str(ntop)+'m_vt'+str(vt[1])+'.dat', names=nam2)
tab3 = pd.read_csv(directory+'mcsnow_ideal_spheric_prof_'+str(r_start)+'_'+str(ntop)+'m_vt'+str(vt[2])+'.dat', names=nam2)
# tab3 = pd.read_csv(directory+'mcsnow_ideal_prof_'        +str(r_start)+'_'+str(ntop)+'m_vt'+str(vt[1])+'_sub.dat', names=nam2)
# print(tab['time'])
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(20,20))
fig.subplots_adjust(wspace=0)
pp      = PdfPages(save_name)

mass, tz, vt, Tphi, igf, rho = [1,2,3,5,4,6] # [1,2,3,4,5,6]         
#--------------------------------------------------------------------------------------------------

ax1  = plt.subplot(row,nmax,mass)
ax1.set_xlabel('$m$ [g]')
ax1.set_ylabel('$z$ [m]')
plt.xscale('log')
plt.ylim(0.0, ntop + ytop)

ax1.plot(tab['m_i']*1000,tab['z'], color=col[0], label=lab[0])
ax1.plot(tab2['m_i']*1000,tab2['z'], color=col[1], label=lab[1])
# ax1.plot(tab3['m_i']*1000,tab3['z'], color=col[2], label=lab[2])
ax1.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax21  = plt.subplot(row,nmax,Tphi,sharey=ax1)
plt.setp(ax21.get_yticklabels(), visible=False)
ax21.set_xlabel('$\u03A6$')
# ax21.set_ylabel('$z$ [m]')
ax21.set_ylim(0.0, ntop + ytop)
ax21.set_xlim(0, max(tab2['phi'])*1.1)

ax21.plot(tab2['phi'],tab2['z'], color=col[1], label='$\u03A6(z)$')
# ax21.plot(tab3['phi'],tab3['z'], color=col[2], label='$\u03A6(z)$')
#--------------------------------------------------------------------------------------------------
ax3  = plt.subplot(row,nmax,igf, sharey=ax1)
# plt.setp(ax3.get_yticklabels(), visible=False)
ax3.set_ylabel('$z$ [m]')
plt.xlabel('IGF' )
ax3.set_xscale('log')
ax3.set_xlim(2e-1, 5)
ax3.set_ylim(0.0, ntop + ytop)

z1     = np.linspace(ntop,0,10001)
T_prof = 273.15 - z1 * 0.0062
inher  = np.zeros(len(T_prof))
h      = [3333,1540,715]

for i in range(len(T_prof)):
    inher[i]= lookup_IGF(T_prof[i])


ax3.annotate('$\\it{prolate}$', xy=(1, 1),  xycoords='data',
            xytext=(0.91, 1.), textcoords='axes fraction',
            horizontalalignment='right', verticalalignment='top',
            fontsize=fsize+2
            )
ax3.annotate('$\\it{oblate}$', xy=(1, 1),  xycoords='data',
            xytext=(0.375, 0.9925), textcoords='axes fraction',
            horizontalalignment='right', verticalalignment='top',
            fontsize=fsize+2
            )

ax3.plot(inher,z1, color=col[1], label='$\u0393(T)$')
ax3.axvline(x=1, linestyle='--', color='grey', linewidth=1.5)
ax3.axvspan(1, 10, facecolor='tab:blue', alpha=0.05)
ax3.axvspan(0,  1, facecolor='tab:red' , alpha=0.05)

for a in range(3):
    ax3.axhline( y=h[a], linestyle='--', color='grey', linewidth=1.5)
    ax21.axhline(y=h[a], linestyle='--', color='grey', linewidth=1.5)

ax32 = ax3.twiny()
ax32.set_xlabel('$T$ [K]')

ax32.plot(T_prof,z1, color = col[0], label= '$T(z)$')
ax32.xaxis.set_ticks_position('bottom') # set the position of the second x-axis to bottom
ax32.xaxis.set_label_position('bottom') # set the position of the second x-axis to bottom
ax32.spines['bottom'].set_position(('outward', fsize*3))

lines, labels = ax3.get_legend_handles_labels()
lines2, labels2 = ax32.get_legend_handles_labels()
ax32.legend(lines + lines2, labels + labels2, loc=0, fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax4  = plt.subplot(row,nmax,tz, sharey=ax1)
plt.setp(ax4.get_yticklabels(), visible=False)

maxt= max(max(tab['t']), max(tab2['t']), max(tab3['t']))
ax4.set_xlim(0, maxt*1.1 )
ax4.set_ylim(0.0, ntop + ytop)
ax4.set_xlabel('$t$ [min]')

ax4.plot(tab['t'],tab['z'], color=col[0], label=lab[0])
ax4.plot(tab2['t'],tab2['z'], color=col[1], label=lab[1])
# ax4.plot(tab3['t'],tab3['z'], color=col[2], label=lab[2])
#--------------------------------------------------------------------------------------------------
ax5  = plt.subplot(row,nmax,rho, sharey=ax1)
plt.setp(ax5.get_yticklabels(), visible=False)
ax5.set_xlim(0, 1010)
ax5.set_ylim(0, ntop + ytop)
ax5.set_xlabel('$\\rho_\mathrm{eff}$ [kg$\,\mathrm{m}^{-3}$]')

ax5.plot(tab2['rho_eff'],tab2['z'], color=col[1], label=lab[1])
ax5.axvline(x=919, linestyle='--', color='grey', linewidth=1.5)

#--------------------------------------------------------------------------------------------------
ax6  = plt.subplot(row,nmax,vt, sharey = ax1)
plt.setp(ax6.get_yticklabels(), visible=False)

ax6.set_xlabel('$v_\mathrm{t}$ [m$\,\mathrm{s}^{-1}$]')
ax6.set_ylim(0.0, ntop + ytop)
ax6.set_xlim(0,max(max(tab['vt']), max(tab2['vt']), max(tab3['vt']))*1.1)

ax6.plot(tab['vt'],tab['z'],   color=col[0], label=lab[0])
ax6.plot(tab2['vt'],tab2['z'], color=col[1], label=lab[1])
# ax6.plot(tab3['vt'],tab3['z'], color=col[2], label=lab[2])
#--------------------------------------------------------------------------------------------------
# ax7  = plt.subplot(row,nmax,7, sharey = ax1)
# # plt.setp(ax6.get_yticklabels(), visible=False)

# # ax7.set_xlabel('$v_\mathrm{t}$ [m$\,\mathrm{s}^{-1}$]')
# ax7.set_ylim(0.0, ntop + ytop)
# ax7.set_xlim(0,max(max(tab2['IGF']),max(tab2['IGF_vent']))*1.1)

# # ax6.plot(tab['vt'],tab['z'],   color=col[0], label=lab[0])
# ax7.plot(tab2['IGF'],tab2['z'], color=col[1], label=lab[1])
# ax7.plot(tab2['IGF_vent'],tab2['z'], color=col[2], label=lab[1])
# ax6.plot(tab3['vt'],tab3['z'], color=col[2], label=lab[2])
#--------------------------------------------------------------------------------------------------

plt.tight_layout()

pp.savefig()

plt.show()
pp.close()
