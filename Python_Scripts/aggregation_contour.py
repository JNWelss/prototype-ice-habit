from plotpack import *
from glob import glob
import math
#--------------------------------------------------------------------------------------------------
directory = '~/leonie_code/mcsnow/experiments/'
var       = ['phi1','phi2', 'vt', 'geom', 'ce_boehm', 'kern']

v         = 3
entries   = 926

z    = np.zeros(shape=(entries-2,entries-2))
a    = np.zeros(shape=(entries-2,entries-2))
vt   = np.zeros(shape=(entries-2,entries-2))
geom = np.zeros(shape=(entries-2,entries-2))
phi1 = np.zeros(shape=(entries-2,entries-2))
phi2 = np.zeros(shape=(entries-2,entries-2))
x    = np.zeros(shape=(entries-2))
#--------------------------------------------------------------------------------------------------
# Plot details
row, col = 2, 2
c        = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 
            'blue','midnightblue', 'slategrey']
labels   = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)']

lines    = {'linewidth':'2.5'}
mp.rc('lines', **lines)
fsize = 40
cmap = cm.get_cmap("jet")
#--------------------------------------------------------------------------------------------------
file = 'check_c2_v'+str(v)+'/colleff_aggregation_phi_0.01-100.dat'

if os.path.exists(directory+file):
  print(directory+file)
else:
    print ("File not exist")
    exit()

frames    = []

os.system('csplit '+directory+file+' '+str(entries-1)+' {'+str(923)+'} -f split. >/dev/null')

os.system('sed "s/^[ \t]*//" -i split.*')

filenames = ['split.00']
for n in range(1,entries-1):
  if n < 10:
    name = 'split.0'+str(n)
  else:
    name = 'split.'+str(n)
  os.system('sed -i 1d '+ name )
  filenames.append(name)

frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = "   ", engine ='python') for f in filenames]) )
result = pd.concat(frames, keys='1')
# print(result[0][2]['phi1'])
# os.system('rm split*')
# exit()
#--------------------------------------------------------------------------------------------------
fig, axes = plt.subplots(figsize=(20*col,20*row), nrows = 1, ncols = col, sharey = True)
fig = plt.figure(figsize=(20*col,20*row))
# fig.subplots_adjust(wspace=0.03, hspace=0)
pp        = PdfPages('./PDFs/coll_efficiency_aggregation.pdf')
#--------------------------------------------------------------------------------------------------
# axes.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
# axes.frameon = False
rc      = {"axes.labelsize"  : fsize,
           "xtick.labelsize" : fsize,
           "xtick.major.size": 18,
           "xtick.minor.size": 12,
           "xtick.major.width": 5,
           "xtick.minor.width": 3,
           "ytick.labelsize" : fsize,
           "ytick.major.size": 18,
           "ytick.minor.size": 12,
           "ytick.major.width": 5,
           "ytick.minor.width": 3,}
plt.rcParams.update(rc)

#--------------------------------------------------------------------------------------------------
ax1 = fig.add_subplot(row,col,1)
ax1.set_title('Coll. Eff. $E_\mathrm{c}$',        fontsize=fsize-2)
ax1.set_xlabel('$\phi_1$', fontsize=30)
ax1.set_ylabel('$\phi_2$', fontsize=30)
ax1.set_xlim(0.01,100)
ax1.set_ylim(0.01,100)
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlabel('$\phi_1$', fontsize=fsize)
ax1.set_ylabel('$\phi_2$', fontsize=fsize)

ax2 = fig.add_subplot(row,col,2)
ax2.set_title('Coll. Kernel $K_\mathrm{c}$',        fontsize=fsize-2)
ax2.set_xlim(0.01,100)
ax2.set_ylim(0.01,100)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlabel('$\phi_1$', fontsize=fsize)
ax2.set_ylabel('$\phi_2$', fontsize=fsize)

ax3 = fig.add_subplot(row,col,3)
ax3.set_title('diff. term. velocity $|\Delta v_\mathrm{t}|$',        fontsize=fsize-2)
ax3.set_xlim(0.01,100)
ax3.set_ylim(0.01,100)
ax3.set_xscale('log')
ax3.set_yscale('log')
ax3.set_xlabel('$\phi_1$', fontsize=fsize)
ax3.set_ylabel('$\phi_2$', fontsize=fsize)

ax4 = fig.add_subplot(row,col,4)
ax4.set_title('Geometry term',        fontsize=fsize-2)
ax4.set_xlim(0.01,100)
ax4.set_ylim(0.01,100)
ax4.set_xscale('log')
ax4.set_yscale('log')
ax4.set_xlabel('$\phi_1$', fontsize=fsize)
ax4.set_ylabel('$\phi_2$', fontsize=fsize)
#--------------------------------------------------------------------------------------------------

i = 0
# for k in range(1,entries-2):
  # x[k-1] = 0.01 * 1.01**(k-1)
  # i = i+1

for i in range(entries-2):
  z[i][:]    = result[0][i]['ce_boehm']
  a[i][:]    = result[0][i]['kern']
  vt[i][:]   = result[0][i]['vt']
  geom[i][:] = result[0][i]['geom']
  phi1[i][:] = result[0][i]['phi1']
  phi2[i][:] = result[0][i]['phi2']

# print(result[0][i-1]['ce_boehm'])
# print(z[:][1])

v = np.linspace(0., 1, 101, endpoint=True)
w = np.linspace(0., np.max(a), 101, endpoint=True)
u = np.linspace(0., np.max(vt), 101, endpoint=True)
g = np.linspace(1e-7, 1e-5, 51, endpoint=True)

#--------------------------------------------------------------------------------------------------
p1 = ax1.contourf(phi1, phi2, z, v, cmap="jet")
cbar1 = fig.colorbar(p1, ax=ax1)
cbar1.ax.tick_params(labelsize=35)
cbar1.ax.set_ylabel("$E_\mathrm{c}$", fontsize=45, labelpad=35, rotation=0)

# a  = np.ma.masked_array(a, mask=(a < 1.e-18))
p2 = ax2.contourf(phi1, phi2, a, w, cmap=cmap)
cbar2 = fig.colorbar(p2, ax=ax2)
cbar2.ax.tick_params(labelsize=35)
cbar2.ax.set_ylabel("$K_\mathrm{c}$", fontsize=45, labelpad=35, rotation=0)
cbar2.formatter.set_powerlimits((0, 0))   # scientific notation 10^

vt = np.ma.masked_array(vt, mask=(vt < 0.001))
p3 = ax3.contourf(phi1, phi2, vt, u, cmap='jet')
cbar3 = fig.colorbar(p3, ax=ax3)
cbar3.ax.tick_params(labelsize=35)
cbar3.ax.set_ylabel("$|\Delta v_\mathrm{t}|$", fontsize=45, labelpad=45, rotation=0)

geom = geom #/ np.max(geom)
p4 = ax4.contourf(phi1, phi2, geom, g, cmap='jet')
cbar4 = fig.colorbar(p4, ax=ax4)
cbar4.ax.tick_params(labelsize=35)
cbar4.ax.set_ylabel("G-Term", fontsize=45, labelpad=45, rotation=0)
#--------------------------------------------------------------------------------------------------
  # plt.scatter(result[0][i]['phi1'], result[0][i]['phi2'], c=result[0][i]['ce_boehm'], cmap='viridis', vmin=0, vmax=1)
#--------------------------------------------------------------------------------------------------
fig.tight_layout(rect=[0, 0.0, 1, 0.98])
pp.savefig(bbox_inches = 'tight', pad_inches = 0.2)
# fig.show()
pp.close()
os.system('rm split*')