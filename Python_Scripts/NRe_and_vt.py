from plotpack import *
from glob import glob
from matplotlib.lines import Line2D
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'
var       = ['d','m','phi','vt','vt_hw','vt_kc']

row       = 1
col       = 1
color     = ['black', 'darkgreen', 'tab:blue']
#--------------------------------------------------------------------------------------------------
file1    = directory+'vt_boehm_vary_phi_cylinder.dat'
file2    = directory+'vt_boehm_vary_phi_prolate.dat'

tab1     = pd.read_csv(file1, delimiter = " ", names=var)
tab2     = pd.read_csv(file2, delimiter = " ", names=var)

savename = 'NRe_and_vt'
png      = True
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(24*col,15*row))
pp  = PdfPages('./PDFs/'+savename+'.pdf')

font    = {'size'   : 34}
mp.rc('font', **font)
#--------------------------------------------------------------------------------------------------
phi1  = tab1["phi"].values
phi2  = tab2["phi"].values
N_Re1 = np.full(phi1.shape, 0.)
N_Re2 = np.full(phi2.shape, 0.)
vt1 = tab1["vt"].values
vt2 = tab2["vt"].values


V = 4./3. * np.pi * 100.e-6**3 
eta = 1.4e-5
rho = 1.

for i in range(0,101):
  if phi1[i] > 1.:
    a1  = (V / (2. * np.pi * phi1[i]) )**(1./3.)
    a2  = ( 0.75 * V / (np.pi * phi2[i]) )**(1./3.) 
  else:
    a2  = ( 0.75 * V / (np.pi * phi2[i]) )**(1./3.) 
    a1  = a2
  b   = a1 * phi1[i]
  r   = ( 0.75 * V / (np.pi) )**(1./3.) 

  N_Re1[i] = vt1[i] * 2. * a1 * rho / eta
  N_Re2[i] = vt2[i] * 2. * a2 * rho / eta

vtmax=max(np.amax(tab1["vt"]),np.amax(tab2["vt"]))
#--------------------------------------------------------------------------------------------------

ax1  = plt.subplot(row, col, 1)
ax2 = ax1.twinx()
ax3 = ax1.twinx()
ax1.set_xlabel('$\phi$')
# ax1.set_ylabel('$v_\mathrm{t} \,[\mathrm{m\,s}^{-1}]$')
ax1.set_ylabel('$\overline{ v_\mathrm{t} }$')
ax1.set_xlim(0.1,10)
ax1.set_ylim(0.48,1.01)
ax1.set_xscale('log')
ax1.yaxis.set_minor_locator(MultipleLocator(.05))
ax1.set_xticklabels(['0','0.1','1','10'])

ax2.set_ylabel('$N_\mathrm{Re}$', color=color[1])
ax2.spines["left"].set_position(("axes", -0.1))
ax2.spines["left"].set_visible(True)
ax2.yaxis.set_label_position('left')
ax2.yaxis.set_ticks_position('left')

ax2.yaxis.set_minor_locator(MultipleLocator(.5))
ax2.tick_params(axis='both', which='major', length=8, width=2)
ax2.tick_params(axis='both', which='minor', length=5, width=2)
ax2.tick_params(axis="y", color=color[1], labelcolor=color[1])
ax2.spines['left'].set_color(color[1])

#--------------------------------------------------------------------------------------------------
ax1.plot(tab1["phi"],tab1["vt"]/vtmax, color=color[0], label='cylinder')
ax1.plot(tab2["phi"],tab2["vt"]/vtmax, color=color[0], label='prolate', linestyle='--')
ax2.plot(phi1,N_Re1, color = color[1], label ='N_Re,cylinder')
ax2.plot(phi2,N_Re2, color = color[1], label ='N_Re,prolate', linestyle='--')

ax1.axvline(x=1., linestyle='--', color='gray')
#--------------------------------------------------------------------------------------------------

custom_lines = [Line2D([0], [0], color=color[0], lw=4),
                Line2D([0], [0], color=color[0], lw=4, linestyle='--'),
                Line2D([0], [0], color=color[1], lw=4),
                Line2D([0], [0], color=color[1], lw=4, linestyle='--')]

ax1.legend(custom_lines, 
  ['$v_\mathrm{t,cylinder}(\phi)$', '$v_\mathrm{t,prolate}(\phi)$', '$N_\mathrm{Re,cyl}$', '$N_\mathrm{Re,pro}$'], 
  bbox_to_anchor=(0, -0.26, 1, 0), frameon=True, loc="lower left", mode="expand", ncol=4)

plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
if png == True:
  plt.savefig('./PDFs/'+savename+'.png', bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
plt.show()