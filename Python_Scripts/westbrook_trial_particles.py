from plotpack import *
from glob import glob
import matplotlib.lines as mlines
import matplotlib

dir       = '~/DWD/Westbrook_Cylinder_Spheroids/'
file_name = 'Supplemental_material-Table_modified.xlsx'

xl_file = pd.ExcelFile('./input_files/'+file_name)
file1   = dir + 'westbrook20_boehm89curve.dat'
file2   = dir + 'boehm/vt_boehm_westbrook_prolate_phi0.10_Ar1.0.dat'
file3   = dir + 'boehm/vt_boehm_westbrook_prolate_phi5.00_Ar1.0.dat'
file3c  = dir + 'boehm/vt_boehm_westbrook_cylinder_phi5.00_Ar1.0.dat'
file4   = dir + 'boehm/vt_boehm_westbrook_prolate_phi1.00_Ar1.0.dat'
var     = ['i', 'm', 'phi', 'v', 'C_d', 'C_d2', 'Re', 'Re_d', 'X', 'q', 'Ar']

df      = pd.read_excel(xl_file, sheet_name=[0,1,2,3], header=2, names=["X","Re","Re_d","C_d","A_r","Motion"])
tab1    = pd.read_csv(file1, delimiter = ",", names=['Re','C_d'])
tab3c   = pd.read_csv(file3c, delimiter = " ", names=var)
# tab2    = pd.read_csv(file2, delimiter = " ", names=var)
# tab3    = pd.read_csv(file3, delimiter = " ", names=var)
# tab4    = pd.read_csv(file4, delimiter = " ", names=var)

phi     = ["0.04", "0.10", "1.00", "2.00", "5.00" ]
Re_West17  = [8.1362, 18.987, 175.37, 328.44]
Cd_West17  = [4.5274, 7.3484, 0.75039, 0.70656]
X_West17   = [299.71, 2649.3, 23079, 76218]
phi_West17 = [1.0476,1.0476,1.0476,1.0476]
# Ar      = [0.2, 0.4, 0.6, 0.8, 1.0]
alls    = []
for n in range(0,5):
    file = dir +  "boehm/vt_boehm_westbrook_prolate_phi"+phi[n]+"_Ar1.0.dat"
    alls.append( pd.read_csv(file, delimiter = " ", names=var) )
# print(alls[1])

plates = df[0].dropna()
cols   = df[1].dropna()
ros    = df[2].dropna()
agg    = df[3].dropna()

alltypes = [plates,cols,ros,agg]

rc      = {"axes.spines.right" : True,
           "axes.spines.top" : True, }
plt.rcParams.update(rc)
font = {'family' : 'sans-serif',
        'weight' : 'normal',
        'size'   : 25}
matplotlib.rc('font', **font)
msize  = 50
labels = ['B89','Plate-like','Column-like','Rosettes', 'Aggregates', 'W & S \'17']
lab2   = ['0.1','1','5','5c']
colors = ['black','tab:red','tab:green','tab:blue','magenta', 'black']
cm     = plt.cm.get_cmap('jet')
#--------------------------------------------------------------------------------------------------
col, row = 1, 5
# fig, ax = plt.subplots(col*row,figsize=(15*col,10*row))
fig = plt.figure(figsize=(15*col,10*row))
pp  = PdfPages('./PDFs/westbrook_data_Re_Cd_comparison_Boehm.pdf')
#--------------------------------------------------------------------------------------------------
ax1 = plt.subplot(row, col, 1)
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(1,10000)
ax1.set_ylim(0.4,25.)
ax1.set_xlabel('$N_\mathrm{Re}$')
ax1.set_ylabel('$C_\mathrm{d}$')
m1=["$+$" if i=="S" else 'o' for i in plates['Motion']]
m2=["$+$" if i=="S" else 'o' for i in cols['Motion']]
m3=["$+$" if i=="S" else 'o' for i in ros['Motion']]
m4=["$+$" if i=="S" else 'o' for i in agg['Motion']]
#--------------------------------------------------------------------------------------------------
ax1.plot(tab1['Re'],tab1['C_d'], c=colors[0], linestyle=':')

for x, y, z, m in zip(plates["Re"],plates["C_d"], plates["A_r"], m1):
    ax1.scatter(x, y, alpha=0.8, edgecolors=colors[1],marker=m, facecolors='none', s=msize)  
for x, y, z, m in zip(cols["Re"],cols["C_d"], cols["A_r"], m2):
    ax1.scatter(x, y, alpha=0.8, edgecolors=colors[2],marker=m, facecolors='none', s=msize) 
for x, y, z, m in zip(ros["Re"],ros["C_d"], ros["A_r"], m3):
    ax1.scatter(x, y, alpha=0.8, edgecolors=colors[3],marker=m, facecolors='none', s=msize)
for x, y, z, m in zip(agg["Re"],agg["C_d"], agg["A_r"], m4):
    ax1.scatter(x, y, alpha=0.8, edgecolors=colors[4],marker=m, facecolors='none', s=msize)
ax1.scatter(Re_West17, Cd_West17, alpha=0.8, edgecolors=colors[5],marker='s', facecolors=colors[0], s=msize, label='Westbrook 17')

line2,=ax1.plot(alls[1]['Re'],alls[1]['C_d'], c=colors[1], linestyle='--')
line3,=ax1.plot(alls[4]['Re'],alls[4]['C_d'], c=colors[2], linestyle='--')
line4,=ax1.plot(alls[2]['Re'],alls[2]['C_d'], c=colors[0], linestyle='--')
cyl, = ax1.plot(tab3c['Re'],tab3c['C_d'], c=colors[2], linestyle=':', label='cylinder')
#--------------------------------------------------------------------------------------------------
ax1.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False,
                     bottom=True, top=False, left=True, right=False)
ax1.tick_params(axis='both', which='major', length=8, width=2, grid_color='r', grid_alpha=0.5)
ax1.tick_params(axis='both', which='minor', length=5, width=2, grid_color='r', grid_alpha=0.5)

ax1.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
ax1.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
#--------------------------------------------------------------------------------------------------
handles = []

handles.append(mlines.Line2D([], [], color=colors[0], marker='None', linestyle=':',
                    markersize=10, label=labels[0]))
for u in range(1,5):
    handles.append(mlines.Line2D([], [], color=colors[u], marker='+', linestyle='None',
                    markersize=10, label=labels[u]) )
handles.append(mlines.Line2D([], [], color=colors[5], marker='s', linestyle='None',
                    markersize=10, label=labels[5]) )

leg = plt.legend(handles=handles, frameon=False)
ax1.add_artist(leg)
plt.legend(handles=[line2,line4,line3,cyl], labels=lab2, loc='lower left', title='$\phi$', frameon=False)
plt.tight_layout(rect=[0, 0.0, 1, 0.9])
pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
#--------------------------------------------------------------------------------------------------
alllines = [alls[1], alls[4], alls[2]]
labprolate = ['prolate', 'cylinder']
hand       = []
for i in range(0,4):
    ax2 = plt.subplot(row, col, i+2)
    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.set_xlim(1,10000)
    ax2.set_ylim(0.4,25.)
    ax2.set_xlabel('$N_\mathrm{Re}$')
    ax2.set_ylabel('$C_\mathrm{d}$')
    markers=["$+$" if l=="S" else 'o' for l in alltypes[i]['Motion']]
    ax2.grid(True,color="grey",which='major',alpha=.3, linewidth=1)
    ax2.grid(True,color="grey",which='minor',alpha=.3, linewidth=1,linestyle=":")
    #----------------------------------------------------------------------------------------------
    for x, y, z, m in zip(alltypes[i]["Re"], alltypes[i]["C_d"], alltypes[i]["A_r"], markers):
        r,g,b,a = cm(z)
        sc = ax2.scatter(x, y, alpha=0.75, c=z, marker=m, facecolors='none', s=30, cmap=cm, vmin=0.1, vmax=0.9)
    if i < 2:
        norm, = ax2.plot(alllines[i]['Re'],alllines[i]['C_d'], c=colors[i+1], linestyle='--')
        if i == 1:
           cyl, = ax2.plot(tab3c['Re'],tab3c['C_d'], c=colors[i+1], linestyle=':')
           plt.legend(handles=[norm,cyl], labels=labprolate, loc='lower left', title='     $\phi = 5$ \n Geometry')
    else:
        ax2.plot(alllines[2]['Re'],alllines[2]['C_d'], c=colors[0], linestyle='--')
    cbar = plt.colorbar(sc, ax=ax2)
    cbar.ax.set_title("$A_\mathrm{r}$")
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    textstr = '\n'.join((
        labels[i+1],))

    ax2.text(0.98, 0.98, textstr, transform=ax2.transAxes, fontsize=fsize-3,
            verticalalignment='top', horizontalalignment='right', bbox=props)
#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
# plt.show()

print('Fin')