from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'

var       = ['T','dN','d1', 'd2']
no        = 10                    # number of curves
#--------------------------------------------------------------------------------------------------
row, col  = 1, 2
# c         = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 
#              'blue','midnightblue', 'slategrey']]
c = ['#322214', '#C55625', '#D6BA73', '#F2C078','#BFB48F','#b81600','#69995D','#7EA3CC','#1A535C','#247BA0','#0A2463'] #8B939C
labels    = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)']
#--------------------------------------------------------------------------------------------------
lines     = {'linewidth':'3.5'}
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 48}
mp.rc('font', **font)
mp.rc('lines', **lines)

#--------------------------------------------------------------------------------------------------
T         = 243
rho_r     = 400.0
file      = 'fragmentation_temp_rhor'+str(rho_r)
savefile  = 'fragmentation_T'+str(T)+'_'+str(T+30)+'_sens_rhor'+str(int(rho_r))

#--------------------------------------------------------------------------------------------------
# Read model data
tab  = pd.read_csv(directory+'check_c2_v3/'+file+'.dat',  delimiter = " ", names=var)
# print(tab['d1'].iloc[0])
#--------------------------------------------------------------------------------------------------
#Plot(s)
fig,ax    = plt.subplots(figsize=(20,15))
pp        = PdfPages(savefile+'.pdf')
#--------------------------------------------------------------------------------------------------
# axis specs
tick_length     = 16
tick_width      = 3
min_tick_length = 8
min_tick_width  = 2
#--------------------------------------------------------------------------------------------------
ax.plot(tab['T'], tab['dN'],   color='black')
ax.set_xlabel('$m$ [kg]')
ax.set_ylabel('$N$')
ax.set_xlim(243.15,273.15)
ax.set_ylim(0.,1000)
ax.yaxis.set_minor_locator(AutoMinorLocator(4))
ax.tick_params(which='major',length=tick_length, width=tick_width )
ax.tick_params(which='minor',length=min_tick_length, width=min_tick_width)
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
# plt.show()
#--------------------------------------------------------------------------------------------------
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
textstr = '\n'.join(( 
    r'$\rho_\mathrm{r}=%4.1f\,\mathrm{kg m}^{-3}$' % (rho_r,),
    # r'$F_\mathrm{r,2}=%2.1f$' % (result.loc['1'].loc[0][0]['Fr1'][0],), 
    r'$d_1=%.3f\,\mathrm{m}$' % (tab['d1'].iloc[0], ),
    r'$d_2=%.3f\,\mathrm{m}$' % (tab['d2'].iloc[0], ),
    r'$F_\mathrm{r,1}=%2.1f$' % (0.5, ),
    r'$F_\mathrm{r,2}=%2.1f$' % (0.5, ))
    )

ax.text(0.05, 0.35, textstr, transform=ax.transAxes, fontsize=40,
        verticalalignment='top', bbox=props)
plt.tight_layout(rect=[0, 0.0, 1, 0.95])
pp.savefig(bbox_inches = 'tight', pad_inches = 0)
# plt.show()
pp.close()