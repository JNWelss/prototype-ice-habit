import os
import numpy as np
import pandas as pd
import matplotlib as mp
mp.use("TkAgg")                 #needed to make .show option work when wrong backend is used
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from functools import reduce
plt.style.use('seaborn-ticks')
#--------------------------------------------------------------------------------------------------
directory = './mcsnow_output/'

names     = ['t','T','a','b','phi','m','V','vt']
nam       = ['mtot','z','vt','d','A','m_i','V_i','phi','rho_eff']
T = 260.95
r_start = 10e-06
dt = 5

colors = ['black', 'darkgray', 'tab:blue']

filename  = 'T'+str(T)+'_p1000.0_dt'+str(dt)+'_r_start'+str(r_start)
#--------------------------------------------------------------------------------------------------
tab  = pd.read_csv(directory+filename+'.out', delimiter = ",", names=names, header = 0)

if dt > 1:
    tab = tab[tab.index % dt == 0]
# exit()


if T == 262.65:
    sets = '2'
    tab2 = pd.read_csv(directory+'mcsnow_output_1e-5_T26265_30min.dat', names=nam)
elif T == 260.95:
    sets = '3'
    tab2 = pd.read_csv(directory+'mcsnow_output_1e-5_T26095_30min.dat', names=nam)

tab2.insert(0, "time", range(1, 1 + len(tab2)), True)

df  = pd.read_csv('./paper_dat/JH_Fig1'+sets+'a.dat', delimiter = ",", names=['t','curve'])
df2 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'b.dat', delimiter = ",", names=['t','curve'])
df3 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'c.dat', delimiter = ",", names=['t','curve'])
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(20,15))
fsize   = 22
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 26}

lines   = {'linewidth':'3',
           'markersize':'5',
           'markeredgewidth':'1.5' }

rc      = rc = {"axes.spines.right" : False,
                "axes.spines.top" : False, }
plt.rcParams.update(rc)

mp.rc('font', **font)
mp.rc('lines', **lines)
plt.suptitle('r_start='+str(r_start*1e6)+' $\mu$m')
#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(131)
plt.xlabel('$t$ [min]')
plt.ylabel('$m$ [g]')

plt.xscale('log')
plt.yscale('log')
plt.xlim(1, 30)
plt.ylim(1e-8, 2e-3)

plt.plot(tab['t']/60,tab['m']*1000,     color=colors[0], label='prototype')
plt.plot(df['t'],df['curve'],           color=colors[1], label='Jensen & Harrington',linestyle='--')
plt.plot(tab2['time'],tab2['m_i']*1000, color=colors[2], label='McSnow',linestyle='--')
ax.set_xticks([1,5,10,30])
ax.set_xticklabels(['1','5','10','30'])
ax.minorticks_off()
plt.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(132)
plt.xlabel('$t$ [min]')
plt.ylabel('$v$ [cm s'r'$^{-1}$]')

plt.xlim(0, 30)
plt.ylim(0, 140)
# plt.title(filename)

plt.plot(tab['t']/60,tab['vt']*100,   color=colors[0], label='prototype')
plt.plot(df2['t'],df2['curve'],       color=colors[1], label='Jensen & Harrington',linestyle='--')
plt.plot(tab2['time'],tab2['vt']*100, color=colors[2], label='McSnow',linestyle='--')
plt.xticks(np.arange(0, 31, 5.0))

#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(133)
plt.xlabel('$t$ [min]')
plt.ylabel('$\u03A6$') #phi

plt.xlim(0, 30)
plt.ylim(0, 1)
# plt.title(filename)

plt.plot(tab['t']/60,tab['phi'],   color=colors[0], label='prototype')
plt.plot(df3['t'],df3['curve'],    color=colors[1], label='Jensen & Harrington 2015',linestyle='--')
plt.plot(tab2['time'],tab2['phi'], color=colors[2], label='McSnow',linestyle='--')
plt.xticks(np.arange(0, 31, 5.0))

#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
# plt.show()

plt.savefig(filename+'compare'+str(T)+'model_output.pdf')
plt.close()