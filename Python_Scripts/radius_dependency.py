import os
import numpy as np
import pandas as pd
import matplotlib as mp
mp.use("TkAgg")                 #needed to make .show option work when wrong backend is used
import matplotlib.pyplot as plt
from   matplotlib.backends.backend_pdf import PdfPages
from   functools import reduce
from   utilities import *
plt.style.use('seaborn-ticks')
#--------------------------------------------------------------------------------------------------
c = ['black', 'tab:blue', 'tab:blue']

ntop      = 3000
ytop      = ntop*0.05 
radi      = [1e-07,1e-06,1e-05]
row       = 2
nmax      = 3

lab = [str(radi[0]), str(radi[1]), str(radi[2])]
lin = ['-','-','--']
save_name = './mcsnow_plots/mcsnow_radius_depend_'+str(ntop)+'m.pdf'
directory = './mcsnow_output/'
nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'IGF_vent']
#--------------------------------------------------------------------------------------------------

tab  = pd.read_csv(directory+'mcsnow_ideal_prof_'+str(radi[0])+'_'+str(ntop)+'m_vt3.dat', names=nam)
tab1 = pd.read_csv(directory+'mcsnow_ideal_prof_'+str(radi[1])+'_'+str(ntop)+'m_vt3.dat', names=nam)
tab2 = pd.read_csv(directory+'mcsnow_ideal_prof_'+str(radi[2])+'_'+str(ntop)+'m_vt3.dat', names=nam)

# print(tab['time'])
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(14,20))
fig.subplots_adjust(wspace=0)

fsize   = 18
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 22}

lines   = {'linewidth':'3',
           'markersize':'5',
           'markeredgewidth':'1.5' }

rc      = rc = {"axes.spines.right" : False,
                "axes.spines.top" : False, }
plt.rcParams.update(rc)

mp.rc('font', **font)
mp.rc('lines', **lines)
# plt.suptitle('r_start='+str(r_start*1e6)+' $\mu$m')
mass, tz, vt, Tphi, igf, rho = 1,2,3,4,5,6 # 1, 6, 2, 3, 5, 4         # 1,2,3,4,5,6

#--------------------------------------------------------------------------------------------------

ax1  = plt.subplot(row,nmax,mass)
plt.xlabel('$m$ [g]')
ax1.set_ylabel('$z$ [m]')

plt.xscale('log')
# plt.xlim(1e-8,max(max(tab['m_i']),max(tab2['m_i']))*1000*1.15)
plt.ylim(0.0, ntop + ytop)

plt.plot(tab['m_i']*1000,tab['z'],   color=c[0], label=lab[0], linestyle=lin[0])
plt.plot(tab1['m_i']*1000,tab1['z'], color=c[1], label=lab[1], linestyle=lin[1])
plt.plot(tab2['m_i']*1000,tab2['z'], color=c[2], label=lab[2], linestyle=lin[2])

plt.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax21  = plt.subplot(row,nmax,Tphi,sharey=ax1)
ax21.set_xlabel('$\u03A6$')
ax21.set_ylabel('$z$ [m]')
# plt.setp(ax21.get_yticklabels(), visible=False)
plt.ylim(0.0, ntop + ytop)
plt.xlim(0, max(max(tab['phi']),max(tab1['phi']),max(tab2['phi']))*1.1)
ax21.plot(tab['phi'], tab['z'],  color=c[0], label=lab[0], linestyle=lin[0])
ax21.plot(tab1['phi'],tab1['z'], color=c[1], label=lab[1], linestyle=lin[1])
ax21.plot(tab2['phi'],tab2['z'], color=c[2], label=lab[2], linestyle=lin[2])

ax21.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax3  = plt.subplot(row,nmax,igf, sharey=ax1)
z1 = np.linspace(5000,0,10001)
T_prof = 273.15 - z1 * 0.0062
inher = np.zeros(len(T_prof))

# h   = np.zeros(100)
h = [3333,1540,715]
# a   = 0

for i in range(len(T_prof)):
    inher[i]= lookup_IGF(T_prof[i])
#     if (inher[i] < 1.002 and inher[i] > 0.998):
#         h[a] = (-(T_prof[i] - 273.15 )/0.0062)
#         print(h[a])
#         a = a+1

plt.xscale('log')
plt.xlim(2e-1, 5)
plt.ylim(0.0, ntop + ytop)

plt.xlabel('IGF' )

ax3.annotate('$\\it{prolate}$', xy=(1, 1),  xycoords='data',
            xytext=(0.91, 1.), textcoords='axes fraction',
            horizontalalignment='right', verticalalignment='top',
            fontsize=fsize+2
            )
ax3.annotate('$\\it{oblate}$', xy=(1, 1),  xycoords='data',
            xytext=(0.375, 0.9925), textcoords='axes fraction',
            horizontalalignment='right', verticalalignment='top',
            fontsize=fsize+2
            )

plt.setp(ax3.get_yticklabels(), visible=False)
plt.plot(inher,z1, color='black', label='no habit')
ax3.axvline(x=1, linestyle='--', color='grey', linewidth=1.5)
plt.axvspan(1, 10, facecolor='tab:blue', alpha=0.05)
plt.axvspan(0,  1, facecolor='tab:red' , alpha=0.05)

for a in range(3):
    ax3.axhline( y=h[a], linestyle='--', color='grey', linewidth=1.5)
    ax21.axhline(y=h[a], linestyle='--', color='grey', linewidth=1.5)

#--------------------------------------------------------------------------------------------------
ax4  = plt.subplot(row,nmax,tz, sharey=ax1)
plt.setp(ax4.get_yticklabels(), visible=False)

plt.xlim(0,max(max(tab['t']), max(tab2['t']))*1.1 )
plt.ylim(0.0, ntop + ytop)
ax4.set_xlabel('$t$ [min]')

plt.plot(tab['t'], tab['z'],  color=c[0], label=lab[0], linestyle=lin[0])
plt.plot(tab1['t'],tab1['z'], color=c[1], label=lab[1], linestyle=lin[1])
plt.plot(tab2['t'],tab2['z'], color=c[2], label=lab[2], linestyle=lin[2])

#--------------------------------------------------------------------------------------------------
ax5  = plt.subplot(row,nmax,rho, sharey=ax1)
# plt.xlabel('vt [m$\,\mathrm{s}^{-1}$]')
plt.xlabel('$\\rho_\mathrm{eff}$ [kg$\,\mathrm{m}^{-3}$]')
plt.setp(ax5.get_yticklabels(), visible=False)
# ax5.set_ylabel('z [m]')

# plt.xscale('log')
# plt.xlim(1e-8,max(max(tab['vt']),max(tab2['vt']))*1000*1.15)
plt.ylim(0, ntop + ytop)
plt.xlim(0, 1010)

plt.plot(tab['rho_eff'],  tab['z'], color=c[0], label=lab[0], linestyle=lin[0])
plt.plot(tab1['rho_eff'],tab1['z'], color=c[1], label=lab[1], linestyle=lin[1])
plt.plot(tab2['rho_eff'],tab2['z'], color=c[2], label=lab[2], linestyle=lin[2])
ax5.axvline(x=919, linestyle='--', color='grey', linewidth=1.5)
# plt.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax6  = plt.subplot(row,nmax,vt, sharey = ax1)
plt.xlabel('$v_\mathrm{t}$ [m$\,\mathrm{s}^{-1}$]')
plt.setp(ax6.get_yticklabels(), visible=False)

plt.ylim(0.0, ntop + ytop)
plt.xlim(0,max(max(tab['vt']), max(tab2['vt']))*1.1)

plt.plot(tab['vt'],tab['z'],   color=c[0], label=lab[0], linestyle=lin[0])
plt.plot(tab1['vt'],tab1['z'], color=c[1], label=lab[1], linestyle=lin[1])
plt.plot(tab2['vt'],tab2['z'], color=c[2], label=lab[2], linestyle=lin[2])
plt.legend(fontsize = fsize)
#--------------------------------------------------------------------------------------------------

plt.tight_layout()

plt.savefig(save_name)

plt.show()
plt.close()
