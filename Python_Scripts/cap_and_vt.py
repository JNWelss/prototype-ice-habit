from plotpack import *
from glob import glob
from matplotlib.lines import Line2D
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'
var       = ['d','m','phi','vt','vt_hw','vt_kc']
vt        = 3

row       = 1
col       = 1
color     = ['black', 'darkgreen', 'tab:blue', 'gray']
#--------------------------------------------------------------------------------------------------
file1    = directory+'vt_boehm_vary_phi_cylinder.dat'
file2    = directory+'vt_boehm_vary_phi_prolate.dat'

tab1     = pd.read_csv(file1, delimiter = " ", names=var)
tab2     = pd.read_csv(file2, delimiter = " ", names=var)

savename = 'cap_and_vt'
png      = False
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(24*col,15*row))
pp  = PdfPages('./PDFs/'+savename+'.pdf')

font    = {'size'   : 34}
mp.rc('font', **font)
#--------------------------------------------------------------------------------------------------
phi = np.logspace(-1.0, 1.0, num=101)
C = np.full(phi.shape, 0.)
f = np.full(phi.shape, 0.)
V = 1000 
X = 10
for i in range(0,101):
  a   = ( 0.75 * V / (np.pi * phi[i]) )**(1./3.) 
  b   = a * phi[i]
  r   = ( 0.75 * V / (np.pi) )**(1./3.) 
  
  if phi[i] < 1.:
    eps = np.sqrt(1. - phi[i]**2)
    C[i]   = a * eps / np.arcsin(eps)
    f[i] = (0.86 + 0.28 * X * (a/r)**0.5)
  elif phi[i] > 1:
    eps = np.sqrt( 1. - phi[i]**-2 )
    C[i]   = b * eps / np.log( (1. + eps) * phi[i] )
    f[i] = (0.86 + 0.28 * X * (b/r)**0.5)
  else:
    C[i]   = (2. * a + b) / 3.
    f[i] = (0.86 + 0.28 * X )


#--------------------------------------------------------------------------------------------------

ax1  = fig.add_subplot(111)
ax1.set_xlabel('$\phi$')
# ax1.set_ylabel('$v_\mathrm{t} \,[\mathrm{m\,s}^{-1}]$')
ax1.set_ylabel('$\overline{ v_\mathrm{t} }$')
ax1.set_xlim(0.1,10)
ax1.set_ylim(0.48,1.01)
ax1.set_xscale('log')
ax1.yaxis.set_minor_locator(MultipleLocator(.05))
# ax1.set_xticklabels(['0','0.1','1','10'])
ax1.spines["right"].set_visible(False)
ax1.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False,
                     bottom=True, top=False, left=True, right=False)


ax2 = ax1.twinx()
# ax3 = ax1.twinx()
ax2.set_ylabel('$\overline{C}$', color=color[3])
ax2.spines["left"].set_position(("axes", -0.1))
ax2.spines["left"].set_visible(True)
ax2.yaxis.set_label_position('left')
ax2.yaxis.set_ticks_position('left')
ax2.spines['right'].set_visible(False)

ax2.yaxis.set_minor_locator(MultipleLocator(.05))
ax2.tick_params(axis='both', which='major', length=8, width=2)
ax2.tick_params(axis='both', which='minor', length=5, width=2)
ax2.tick_params(axis="y", color=color[3], labelcolor=color[3])
ax2.spines['left'].set_color(color[3])

# ax3.set_ylabel('$\overline{f}$', color=color[2])
# ax3.spines["left"].set_position(("axes", -0.2))
# ax3.spines["left"].set_visible(True)
# ax3.yaxis.set_label_position('left')
# ax3.yaxis.set_ticks_position('left')
# ax3.yaxis.set_minor_locator(MultipleLocator(.05))
# ax3.tick_params(axis='both', which='major', length=8, width=2)
# ax3.tick_params(axis='both', which='minor', length=5, width=2)
# ax3.tick_params(axis="y", color=color[2], labelcolor=color[2])
# ax3.spines['left'].set_color(color[2])
# vtmax=max(np.amax(tab1["vt"]),np.amax(tab2["vt"]))
#--------------------------------------------------------------------------------------------------
ax1.plot(tab1["phi"],tab1["vt"]/np.amax(tab1["vt"]), color=color[0], label='cylinder')
ax1.plot(tab2["phi"],tab2["vt"]/np.amax(tab2["vt"]), color=color[0], label='prolate', linestyle='--')
ax2.plot(phi,C/C[50], color = color[3], label ='capacitance', linestyle='-.')
# ax3.plot(phi,f/f[50], color = color[2], label ='ventilation', linestyle='-.')
# leg1 = ax1.legend(fontsize = fsize-3)
# leg2 = ax2.legend(fontsize = fsize-3)
ax1.axvline(x=1., linestyle=':', color='gray')
#--------------------------------------------------------------------------------------------------

custom_lines = [Line2D([0], [0], color=color[0], lw=4),
                Line2D([0], [0], color=color[0], lw=4, linestyle='--'),
                Line2D([0], [0], color=color[3], lw=4, linestyle='-.')]#,

ax1.legend(custom_lines, 
  ['$v_\mathrm{t,cylinder}(\phi)$', '$v_\mathrm{t,prolate}(\phi)$', '$\overline{C}\,(\phi)$', '$\overline{f}\,(\phi)$'], 
  bbox_to_anchor=(0, -0.26, 1, 0), frameon=True, loc="lower left", mode="expand", ncol=4)

plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
if png == True:
  plt.savefig('./PDFs/'+savename+'.png', bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
plt.show()