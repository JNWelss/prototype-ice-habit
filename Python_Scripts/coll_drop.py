from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '~/mcsnow/experiments/'
var       = ['d1','d2','ce_boehm','ce_bulk','ce_spheres','rho_rime','vel_rel','St','N_Re',
             'N_ReBig','ce_BG','ce_Robin','ce_Slinn','ce_Holger']
rime_frac = ['000','100']
vt        = [2,3]
no        = 8
dir_n     = ['1']
dir_lab   = ['0.2','0.5','1','2','5']
case      = 'simple hexagonal plates'
#--------------------------------------------------------------------------------------------------
for v in vt:
    frames    = []
    for rime in rime_frac:
        for phi in range(len(dir_n)):
            os.system('csplit '+directory+'check_c2_v'+str(v)+'_phi'+str(dir_n[phi])+'/colleff_riming_Fr'+str(rime)+'_rhor600_drop_spectrum.dat \
                   201 {'+str(no-1)+'} -f Fr'+str(rime)+'. >/dev/null')
            os.system('sed "s/^[ \t]*//" -i Fr'+str(rime)+'.*')

            filenames = ['Fr'+str(rime)+'.00']
            for n in range(1,no):
                name = 'Fr'+str(rime)+'.0'+str(n)
                os.system('sed -i 1d '+ name )
                filenames.append(name)

            frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenames]) )

#--------------------------------------------------------------------------------------------------
        result = pd.concat(frames, keys=dir_n)
#--------------------------------------------------------------------------------------------------
        row, col = 1, 3
        c        = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 'blue']
        labels   = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)']
        #--------------------------------------------------------------------------------------------------
        fig, axes = plt.subplots(figsize=(10*col,10*row), nrows = 1, ncols = 1)#, sharey = True)
        fig.subplots_adjust(wspace=0.03, hspace=0)
        fig.suptitle(case+', Fr'+rime+', vt'+str(v), fontsize=16)
        pp        = PdfPages('./mcsnow_plots/coll_eff/coll_efficiency_all_v'+str(v)+'_Fr'+rime+'_drops_plates.pdf')
        lines     = {'linewidth':'2.5'}

        mp.rc('lines', **lines)
#--------------------------------------------------------------------------------------------------
        axes.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
        axes.frameon = False
#--------------------------------------------------------------------------------------------------
        # subplots
        for k in range(0,len(dir_n)):
            ax  = fig.add_subplot(row,col,1)
            ax.set_title( 'Cober & List', fontsize=fsize-2)
            ax.set_xlabel('drop radius $D$ [ym]')
            ax.set_ylabel('collision efficiency')
            ax.set_xlim(0,100)
            ax.set_xticks(np.arange(0, 110, 10.0))

            ax1 = fig.add_subplot(row,col,2, sharey=ax)
            ax1.set_title('Boehm',        fontsize=fsize-2)
            ax1.set_xlabel('drop radius $D$ [ym]')
            ax1.set_xlim(0,100)
            ax1.set_ylim(0.0,1.01)
            ax1.set_xticks(np.arange(0, 110, 10.0))

            ax2 = fig.add_subplot(row,col,3)
            ax2.set_title('N_Re',        fontsize=fsize-2)
            ax2.set_xlabel('drop radius $D$ [ym]')
            ax2.set_xlim(0,100)
            ax2.set_ylim(0.0,100)
            ax2.set_xticks(np.arange(0, 110, 10.0))

            plt.setp(ax1.get_yticklabels(), visible=False)
            plt.setp(ax2.get_yticklabels(), visible=False)

            for i in range(no):
                ax.plot( result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_bulk'],  
                         color=c[i],label=labels[i])
                ax1.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_boehm'], 
                         color=c[i],label=labels[i]+', $N_\mathrm{Re}$'+str(round(result.loc[dir_n[0]].loc[i][0]['N_ReBig'][0], 2) ) )
                ax2.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['N_Re'], 
                         color=c[i])
            ax1.legend(fontsize = fsize-3)
#--------------------------------------------------------------------------------------------------
        plt.tight_layout(rect=[0, 0.0, 1, 0.95])
        pp.savefig()
        # plt.show()
        pp.close()
        os.system('rm Fr'+str(rime)+'*')