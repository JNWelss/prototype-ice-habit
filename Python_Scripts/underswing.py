from plotpack import *
#--------------------------------------------------------------------------------------------------
directory = './mcsnow_output_riming/'

names     = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'mm', 'm_r', 'V_r']

T = 262.65
r_start = 10e-06
dt = 5

lwc       = '04'
vt        = '2'

colors = ['black', 'darkgray', 'tab:blue', 'forestgreen', 'orange']
lab    = [lwc+'$\,\mathrm{gm}^{-3}$', 'J&H 15 depo', 
          'J&H 15 $0.4\,\mathrm{gm}^{-3}$', 'J&H 15 $2.0\,\mathrm{gm}^{-3}$' ]

filename  = 'mass2fr_'+lwc+'lwc_vt'+vt
save_name = './mcsnow_plots/underswing_'+lwc+'lwc_vt'+vt+'.pdf'
#--------------------------------------------------------------------------------------------------
tab  = pd.read_csv(directory+filename+'.dat',  delimiter = ",", names=names, header = 0)
# if dt > 1:
#     tab = tab[tab.index % dt == 0]
# exit()

sets = '2'


df  = pd.read_csv('./paper_dat/JH_Fig1'+sets+'a.dat', delimiter = ",", names=['t','curve'])
df2 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'b.dat', delimiter = ",", names=['t','curve'])
df3 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'c.dat', delimiter = ",", names=['t','curve'])

dd  = pd.read_csv('./paper_dat/JH_Fig12a_04gm-3.dat', delimiter = ",", names=['t','curve'])
dd2 = pd.read_csv('./paper_dat/JH_Fig12b_04gm-3.dat', delimiter = ",", names=['t','curve'])
dd3 = pd.read_csv('./paper_dat/JH_Fig12c_04gm-3.dat', delimiter = ",", names=['t','curve'])

de  = pd.read_csv('./paper_dat/JH_Fig12a_2gm-3.dat', delimiter = ",", names=['t','curve'])
de2 = pd.read_csv('./paper_dat/JH_Fig12b_2gm-3.dat', delimiter = ",", names=['t','curve'])
de3 = pd.read_csv('./paper_dat/JH_Fig12c_2gm-3.dat', delimiter = ",", names=['t','curve'])
#--------------------------------------------------------------------------------------------------
fig     = plt.figure(figsize=(20,15))
fsize   = 22
pp      = PdfPages(save_name)
# plt.suptitle('r_start='+str(r_start*1e6)+' $\mu$m')

#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(131)
plt.xlabel('$t$ [min]')
plt.ylabel('$m$ [g]')

plt.xscale('log')
plt.yscale('log')
plt.xlim(1, 30)
plt.ylim(1e-8, 2e-3)

plt.plot(tab['t'], tab['mtot'] *1000,   color=colors[0], label=lab[0])
plt.plot(df['t'],  df['curve'],         color=colors[1], label=lab[1],linestyle='--')
plt.plot(dd['t'],  dd['curve'],         color=colors[3], label=lab[2],linestyle='--')
plt.plot(de['t'],  de['curve'],         color=colors[4], label=lab[3],linestyle='--')
ax.set_xticks([1,5,10,30])
ax.set_xticklabels(['1','5','10','30'])
ax.minorticks_off()
plt.legend(fontsize = fsize)

#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(132)
plt.xlabel('$t$ [min]')
plt.ylabel('$v$ [cm s'r'$^{-1}$]')

plt.xlim(0, 30)
plt.ylim(0, 140)
# plt.title(filename)

plt.plot(tab['t'], tab['vt']*100,   color=colors[0], label=lab[0])
plt.plot(df2['t'],df2['curve'],     color=colors[1], label=lab[1],linestyle='--')
plt.plot(dd2['t'],dd2['curve'],     color=colors[3], label=lab[2],linestyle='--')
plt.plot(de2['t'],de2['curve'],     color=colors[4], label=lab[3],linestyle='--')
plt.xticks(np.arange(0, 31, 5.0))

#--------------------------------------------------------------------------------------------------
ax  = plt.subplot(133)
plt.xlabel('$t$ [min]')
plt.ylabel('$\u03A6$') #phi

plt.xlim(0, 30)
plt.ylim(0, 1)
# plt.title(filename)

plt.plot(tab['t'],tab['phi'],   color=colors[0], label=lab[0])
plt.plot(df3['t'],df3['curve'], color=colors[1], label=lab[1],linestyle='--')
plt.plot(dd3['t'],dd3['curve'], color=colors[3], label=lab[2],linestyle='--')
plt.plot(de3['t'],de3['curve'], color=colors[4], label=lab[3],linestyle='--')
plt.xticks(np.arange(0, 31, 5.0))

#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.03, 1, 0.95])
# plt.show()

pp.savefig()
plt.show()
pp.close()