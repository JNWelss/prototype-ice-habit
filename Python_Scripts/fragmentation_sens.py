from plotpack import *
from glob import glob
import matplotlib.ticker as ticker
#--------------------------------------------------------------------------------------------------
directory = '~/leonie_code/mcsnow/experiments/'

var       = ['d1', 'd2', 'dN', 'CKE', 'delta_v', 'm1', 'm2', 'Fr1', 'Fr2']
no        = 10                    # number of curves
#--------------------------------------------------------------------------------------------------
row, col  = 1, 2
# c         = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 
#              'blue','midnightblue', 'slategrey']]
c = ['#322214', '#C55625', '#D6BA73', '#F2C078','#BFB48F','#b81600','#69995D','#7EA3CC','#1A535C','#247BA0','#0A2463'] #8B939C
labels    = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)']
#--------------------------------------------------------------------------------------------------
lines     = {'linewidth':'3.5'}
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 48}
mp.rc('font', **font)
mp.rc('lines', **lines)
#--------------------------------------------------------------------------------------------------
T         = 257.6
rho_r     = 400.0 
file      = 'fragmentation_test_T'+str(T)+'_rhor'+str(rho_r)
savefile  = 'fragmentation_test_T'+str(int(T))+'_rhor'+str(int(rho_r))
#--------------------------------------------------------------------------------------------------
# Read model data
frames    = []
os.system('csplit '+directory+'check_c2_v3/'+file+'.dat \
                       701 {'+str(no)+'} -f frag. >/dev/null')
os.system('sed "s/^[ \t]*//" -i frag.*')

filenames = ['frag.00']
for n in range(0,no):
  if n < 10:
    name = 'frag.0'+ str(n)
  else:
    name = 'frag.'+ str(n)
  os.system('sed -i 1d '+ name )
  filenames.append(name)

frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenames]) )
result = pd.concat(frames, keys=['1'])
#--------------------------------------------------------------------------------------------------
# framesx    = []
# os.system('csplit '+directory+'check_c2_v3/fragmentation_test_T265.1.dat \
#                        501 {'+str(no-1)+'} -f frag1. >/dev/null')
# os.system('sed "s/^[ \t]*//" -i frag1.*')

# filenamesx = ['frag1.00']
# for n in range(1,no):
#   namex = 'frag1.0'+str(n)
#   os.system('sed -i 1d '+ namex )
#   filenamesx.append(namex)

# framesx.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenamesx]) )
# resultx = pd.concat(framesx, keys=['1'])

#--------------------------------------------------------------------------------------------------
#Plot(s)
fig, axes = plt.subplots(figsize=(25*col,25*row), nrows = 1, ncols = 1, sharey = True)
fig.subplots_adjust(wspace=0.1, hspace=0)
pp        = PdfPages('./PDFs/'+savefile+'.pdf')
#--------------------------------------------------------------------------------------------------
axes.tick_params(labelcolor=(0.,0.,0., 0.0), color='white', which='both', top='off', bottom='off', left='off', right='off')
axes.frameon = False
#--------------------------------------------------------------------------------------------------
# axis specs
tick_length     = 20
tick_width      = 4
min_tick_length = 10
min_tick_width  = 3

no_min_ticks_y  = 9
#--------------------------------------------------------------------------------------------------
ax1 = fig.add_subplot(row,col,1)
# ax1.set_title('T = 263.1', fontsize=fsize-2)
ax1.set_xlabel('$m$ [kg]')
ax1.set_ylabel('$N$')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(1e-6,3.15e-4)
ax1.set_ylim(1,1000)
# ax1.yaxis.set_minor_locator(ticker.LogLocator(base=10.0,numticks=15))
ax1.tick_params(which='major',length=tick_length,     width=tick_width )
ax1.tick_params(which='minor',length=min_tick_length, width=min_tick_width)
ax1.tick_params(axis='x',which='both', top=False)
ax1.tick_params(axis='y',which='both', right=False, left=True)

ax2 = fig.add_subplot(row,col,2)
# ax2.set_title('T = 265.1', fontsize=fsize-2)
ax2.set_xlabel('$d$ [m]')
# ax2.set_ylabel('$dN$')
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlim(1e-4,1.25e-2)
ax2.set_ylim(1,1000)
ax2.set_yticklabels([])
# ax2.yaxis.set_minor_locator(ticker.LogLocator(base=10.0,numticks=15))
ax2.tick_params(which='major',length=tick_length,     width=tick_width )
ax2.tick_params(which='minor',length=min_tick_length, width=min_tick_width)
ax2.tick_params(axis='x',which='both', top=False)
ax2.tick_params(axis='y',which='both', right=False, left=True)

for i in range(0,no+1):
  l1 = ax1.plot(result.loc['1'].loc[i][0]['m1'],result.loc['1'].loc[i][0]['dN'], 
                color=c[i],label='$F_\mathrm{r,1} = $' + str(i/10.) )
  l2 = ax2.plot(result.loc['1'].loc[i][0]['d1'],result.loc['1'].loc[i][0]['dN'], 
                color=c[i],label='$F_\mathrm{r,1} = $' + str(i/10.) )
  # l1 = ax1.plot(result.loc['1'].loc[i][0]['m1'],result.loc['1'].loc[i][0]['dN'], 
  #               color=c[i],label='T = ' + str(258.15+i)+ ' K') 
  # l2 = ax2.plot(result.loc['1'].loc[i][0]['d1'],result.loc['1'].loc[i][0]['dN'], 
  #               color=c[i],label='T = ' + str(258.15+i)+ ' K')
  # l2 = ax2.plot(resultx.loc['1'].loc[i][0]['m1'],resultx.loc['1'].loc[i][0]['dN'], 
                # color=c[i],label='Fr =' + str(i/10.) )
leg1 = ax1.legend(fontsize = 40, loc=2, labelspacing=.2)
#--------------------------------------------------------------------------------------------------
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
textstr = '\n'.join((
    'specifications:\n' 
    r'$\rho_\mathrm{r}=%4.1f\,\mathrm{kg m}^{-3}$' % (rho_r,),
    r'$T=%4.1f\,\mathrm{K}$' % (T,),
    # r'$F_\mathrm{r,2}=%2.1f$' % (result.loc['1'].loc[0][0]['Fr1'][0],), 
    'collision partner:\n'
    r'$d_2=%.3f\,\mathrm{m}$' % (result.loc['1'].loc[0][0]['d2'][0], ),
    r'$F_\mathrm{r,2}=%2.1f$' % (result.loc['1'].loc[0][0]['Fr2'][0], ))
    )

ax2.text(0.05, 0.95, textstr, transform=ax2.transAxes, fontsize=40,
        verticalalignment='top', bbox=props)
plt.tight_layout(rect=[0, 0.0, 1, 0.95])
pp.savefig(bbox_inches = 'tight', pad_inches = 0)
# plt.show()
pp.close()
os.system('rm frag.*')