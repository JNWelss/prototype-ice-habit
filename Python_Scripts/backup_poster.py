from plotpack import *
from glob import glob

#--------------------------------------------------------------------------------------------------
col = ['black','tab:blue','forestgreen']
# markers = [".",",","o","v","^","<",">","1","2","3","4","8","s","p","P","*","h","H","+","x","X","D","d","|","_",0,1,2,3,4,5,6,7,8,9,10,11]
markers=['^','<','*']
ntop      = 5000
# ytop      = ntop*0.05 
# r_start   = 1e-06
vt        = [3]
row       = 1
nmax      = 1

no        = 27

save_name = './poster_multi_part_'+str(ntop)+'m_vt'+str(vt[0])+'.pdf'
# directory = './mcsnow_output_riming/'
directory = '~/mcsnow/experiments/1d_habit_xi100000_nz500_lwc20_ncl0_dtc5_nrp200_rm10_rt0_vt3_h10-20_ba500_habit1/'

nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'mm', 'm_r', 'V_r']
#--------------------------------------------------------------------------------------------------
df  = pd.read_csv(directory+'mass2fr.dat', names=nam)
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(18,18))
fig.subplots_adjust(wspace=0, hspace=0)
pp      = PdfPages(save_name)

#--------------------------------------------------------------------------------------------------
ax1  = plt.subplot(row,nmax,1)

ax1.set_xlabel('$\overline{t}$')
ax1.set_ylabel('$z$ [m]')
# ax1.set_xlim(0.,1)#max(df['t']) )

color=cm.viridis(np.linspace(0,1,no))

# for i,c in zip(range(2,no),color):
lim = [0.,0.8,1.2,100]
for i in range(1,no):

  phi = df.loc[df.mm == i, 'phi'].values
  t = df.loc[df.mm == i,'t']
  z = df.loc[df.mm == i,'z']
  # m   = np.empty(phi.shape, dtype=str)
  # m = np.full(phi.shape, 'green')

  # m[phi > 1.25] = 'blue'
  # m[phi < 0.8]  = 'red'

  for idx, marker in enumerate(markers):  
    j = np.full(phi.shape, False)
    j = ( (phi > lim[idx]) & (phi < lim[idx+1]) )
    ax1.scatter( t[j][::3], z[j][::3], color=color[i], marker=marker, s=120)
  ax1.plot( t, z, color=color[i], linewidth=2)

# ax1.legend(fontsize = fsize)
#--------------------------------------------------------------------------------------------------

plt.tight_layout()

pp.savefig()

plt.show()
pp.close()
