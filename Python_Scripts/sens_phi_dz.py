from plotpack import *
from glob import glob
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm, LogNorm, LinearSegmentedColormap
import matplotlib.lines as mlines
#--------------------------------------------------------------------------------------------------
# markers = [".",",","o","v","^","<",">","1","2","3","4","8","s","p","P","*","h","H","+","x","X","D","d","|","_",0,1,2,3,4,5,6,7,8,9,10,11]
markers=['o','^','+', 'x']
ntop      = 3000
# ytop      = ntop*0.05 
# r_start   = 1e-06
vt        = [3]
row       = 1
nmax      = 3

no        = 10

save_name = './PDFs/sens_phi_dz.pdf'
# directory = './mcsnow_output_riming/'
directory = '/home/jan/setup_leonie/McSnow_habit/experiments/'
dat1      = '1d_2_dendrite_habit1_frag0_xi100_nz300_lwc0_iwc0.1_nugam1._mugam1._dtc5_nucltype0_nrp2_rm10_rt0_vt3_coll_kern1_at2_stick2_dt1_geo3_h10-_ba500_domtop3000._atmo1/'
dat2      = '1d_2_dendrite_habit1_frag0_xi100_nz100_lwc0_iwc0.1_nugam1._mugam1._dtc5_nucltype0_nrp2_rm10_rt0_vt3_coll_kern1_at2_stick2_dt1_geo3_h10-_ba500_domtop3000._atmo1/'
dat3      = '1d_2_dendrite_habit1_frag0_xi100_nz60_lwc0_iwc0.1_nugam1._mugam1._dtc5_nucltype0_nrp2_rm10_rt0_vt3_coll_kern1_at2_stick2_dt1_geo3_h10-_ba500_domtop3000._atmo1/'
dat4      = '1d_2_dendrite_habit1_frag0_xi100_nz30_lwc0_iwc0.1_nugam1._mugam1._dtc5_nucltype0_nrp2_rm10_rt0_vt3_coll_kern1_at2_stick2_dt1_geo3_h10-_ba500_domtop3000._atmo1/'

nam       = ['t','mtot','z','vt','d','A','m_i','V_i','phi','rho_eff', 'IGF', 'mm', 'm_r', 'V_r', 'xi']
#--------------------------------------------------------------------------------------------------
df1 = pd.read_csv(directory+dat1+'mass2fr.dat', names=nam, header=0)
df2 = pd.read_csv(directory+dat2+'mass2fr.dat', names=nam, header=0)
df3 = pd.read_csv(directory+dat3+'mass2fr.dat', names=nam, header=0)
df4 = pd.read_csv(directory+dat4+'mass2fr.dat', names=nam, header=0)
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(nmax*10,15))
fig.subplots_adjust(wspace=0, hspace=0)
pp      = PdfPages(save_name)
ax1  = plt.subplot(row,nmax,1)
ax2  = plt.subplot(row,nmax,2)
ax3  = plt.subplot(row,nmax,3)
# print(df1.loc[df1.xi==1,'phi'])
ax1.set_xticks(np.arange(0, 3.1, step=1.0))
ax2.set_xticks(np.arange(0, 3.1, step=1.0))
ax3.set_xticks(np.arange(0, 3.1, step=1.0))
#--------------------------------------------------------------------------------------------------
msize  = 75
colors = cm.rainbow(np.linspace(0, 1, 10) )
ax1.set_xlim(0.9,3.1)
ax1.set_ylim(min(np.min(df1.phi),np.min(df2.phi))*0.98,max(np.max(df1.phi),np.max(df2.phi))*1.01 )

ax1.set_xlabel("$\overline{t}$")
ax1.set_ylabel("$\phi$")

ax2.set_xlabel("$\overline{t}$")
ax2.set_ylabel("$\Delta \phi [\%]$")

ax3.set_xlabel("$\overline{t}$")
ax3.set_ylabel("$\Delta m[\%]$")

fig.suptitle('$z_\mathrm{top}=3\,\mathrm{km}, r_\mathrm{start}=10\,\mu \mathrm{m}, \Delta t = 30\,\mathrm{min}$, stand. atmos.', fontsize=25)
#--------------------------------------------------------------------------------------------------
for i, c in zip(range(1,no+1), colors):

  phi1 = df1.loc[df1.xi == i, 'phi'].values
  phi2 = df2.loc[df2.xi == i, 'phi'].values
  phi3 = df3.loc[df3.xi == i, 'phi'].values
  phi4 = df4.loc[df4.xi == i, 'phi'].values
  m1   = df1.loc[df1.xi == i, 'mtot'].values
  m2   = df2.loc[df2.xi == i, 'mtot'].values
  m3   = df3.loc[df3.xi == i, 'mtot'].values
  m4   = df4.loc[df4.xi == i, 'mtot'].values
  t   = df1.loc[df1.xi == i,'t'] / 30
  # t4  = df4.loc[df4.xi == i,'t'] / 30
  z   = df1.loc[df1.xi == i,'z']

  # print(c)
  ax1.scatter( t[:], phi1[:], color=c, marker=markers[0], zorder=1, s=msize)
  ax1.scatter( t[:], phi2[:], color=c, marker=markers[1], zorder=1, s=msize)
  ax1.scatter( t[:], phi3[:], color=c, marker=markers[2], zorder=1, s=msize)
  # ax1.scatter( t4[:],phi4[:], color=c, marker=markers[3], zorder=1, s=msize)
  ax2.scatter( t[:], phi2[:]/phi1[:]*100. - 100., color=c, marker='s', zorder=1, s=msize)
  ax2.scatter( t[:], phi3[:]/phi1[:]*100. - 100., color=c, marker='*', zorder=1, s=msize)
  # ax2.scatter( t[:], phi4[:3]/phi1[:]*100. - 100., color=c, marker='x', zorder=1, s=msize)
  ax3.scatter( t[:], m2[:]/m1[:]*100.-100, color=c, marker='s', zorder=1, s=msize)
  ax3.scatter( t[:], m3[:]/m1[:]*100.-100, color=c, marker='*', zorder=1, s=msize)
  # ax3.scatter( t[:], m4[:3]/m1[:]*100.-100, color=c, marker='x', zorder=1, s=msize)
#--------------------------------------------------------------------------------------------------
fif = mlines.Line2D([], [], color='black', marker=markers[2], linestyle='None',
                          markersize=15, label='$50\,\mathrm{m}$')
thir = mlines.Line2D([], [], color='black', marker=markers[1], linestyle='None',
                          markersize=15, label='$30\,\mathrm{m}$')
ten = mlines.Line2D([], [], color='black', marker=markers[0], linestyle='None',
                          markersize=15, label='$10\,\mathrm{m}$')

ax1.legend(title="$\Delta z$",handles=[fif,thir,ten], fontsize=28, labelspacing=.3, handletextpad=0.1)

twotoone = mlines.Line2D([], [], color='black', marker='s', linestyle='None',
                          markersize=15, label='$30\,: 10$')
threetoone = mlines.Line2D([], [], color='black', marker='*', linestyle='None',
                          markersize=15, label='$50\,: 10$')

ax2.legend(title="$\mathrm{ratio} \Delta z$",handles=[twotoone,threetoone], fontsize=28, labelspacing=.3, handletextpad=0.1)

plt.tight_layout(rect=[0, 0.0, 1, 0.95])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)

# plt.show()
pp.close()
