from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------

directory = '~/leonie_code/mcsnow/experiments/'
var       = ['d1','d2','ce_boehm','kernel', 'v1', 'v2', 'Re']
vt        = [3]
no        = 1
cas       = 2
norm      = 100
#--------------------------------------------------------------------------------------------------

# for v in vt:
  # pres = 1000
  # os.system('csplit '+directory+'check_c2_v'+str(v)+'/colleff_pressure_dep.dat            201 {2} -f p'+str(pres)+'. >/dev/null')
  # os.system('sed "s/^[ \t]*//" -i p'+str(pres)+'.*')
#--------------------------------------------------------------------------------------------------
for cas in range(0,3):
  if cas == 0:
    case     = './PDFs/pressure_dependency_water_'+str(norm)+'ym.pdf'
    files    = ['drops_p1000', 'drops_p750', 'drops_p500']
    # files    = ['cold_p1000', 'cold_p750', 'cold_p500']
    limit    = [1e-1, 1.2, 80, 140]
  elif cas == 1:
    case     = './PDFs/pressure_dependency_phi05_'+str(norm)+'ym.pdf'
    files    = ['oblate_p1000', 'oblate_p750', 'oblate_p500']  
    limit    = [1e-1, 1.2, 80, 120]
  elif cas == 2:
    case     = './PDFs/pressure_dependency_phi3_'+str(norm)+'ym.pdf'
    files    = ['prolate_p1000', 'prolate_p750', 'prolate_p500']  
    limit    = [1e-1, 1.20, 80, 120]
  elif cas == 3:
    case     = './PDFs/pressure_dependency_phi_cross_'+str(norm)+'ym.pdf'
    files    = ['cross.00', 'cross.01', 'cross.02']  
    limit    = [1e-3, 1.20, 0, 120]

  tab1     = pd.read_csv("./input_files/"+files[0]+".dat", delimiter = " ", names=var, skiprows=[0])
  tab2     = pd.read_csv("./input_files/"+files[1]+".dat", delimiter = " ", names=var, skiprows=[0])
  tab3     = pd.read_csv("./input_files/"+files[2]+".dat", delimiter = " ", names=var, skiprows=[0])

  # os.system('rm p1000'+'*')
  #--------------------------------------------------------------------------------------------------
  row = 2
  col = 2
  fig = plt.figure(figsize=(8*col,8*row))
  fig.subplots_adjust(wspace=0, hspace=0)

  pp      = PdfPages(case)

  font = {'size'   : 34}   
  #--------------------------------------------------------------------------------------------------

  ax1 = plt.subplot( row, col, 1)
  ax1.set_xlabel('$d_2$ / $d_1$')

  ax1.set_ylabel('$E_\mathrm{c} }$')
  ax1.set_xlim(0.,1)
  ax1.set_ylim(limit[0],limit[1])
  ax1.set_yscale('log')
  ax1.tick_params(axis='both', which='major', length=8, width=2)
  ax1.tick_params(axis='both', which='minor', length=5, width=2)
  #--------------------------------------------------------------------------------------------------
  ax1.plot(tab1["d2"]/tab1["d1"],tab1["ce_boehm"], color='black', label='$E_\mathrm{c,1000 hPa}$')
  ax1.plot(tab2["d2"]/tab2["d1"],tab2["ce_boehm"], color='green', label='$E_\mathrm{c,750 hPa}$')
  ax1.plot(tab3["d2"]/tab3["d1"],tab3["ce_boehm"], color='blue',  label='$E_\mathrm{c,500 hPa}$')
  ax1.legend()

  # props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
  # textstr = '\n'.join((
  #     'specifications:\n' 
  #     r'$d_1 = 50\,\mu\mathrm{m}$' 
  #     ) )

  # ax1.text(0.05, 0.95, textstr, transform=ax1.transAxes, fontsize=20, verticalalignment='top', bbox=props)
  #--------------------------------------------------------------------------------------------------
  ax2 = plt.subplot( row, col, 2)
  ax2.set_xlabel('$d_2$ / $d_1$')

  ax2.set_ylabel('relative $E_\mathrm{c}$ [%]')
  ax2.set_xlim(0.,1)
  ax2.set_ylim(limit[2],limit[3])
  ax2.yaxis.set_minor_locator(MultipleLocator(10))
  ax2.yaxis.set_major_locator(MultipleLocator(20))
  ax2.tick_params(axis='both', which='major', length=8, width=2)
  ax2.tick_params(axis='both', which='minor', length=5, width=2)

  #--------------------------------------------------------------------------------------------------
  ax2.plot(tab2["d2"]/tab2["d1"],tab2["ce_boehm"]/tab1["ce_boehm"]*100, color='green', label='$E_\mathrm{c,750 hPa}$ / $E_\mathrm{c,1000 hPa}$')
  ax2.plot(tab3["d2"]/tab3["d1"],tab3["ce_boehm"]/tab1["ce_boehm"]*100, color='blue',  label='$E_\mathrm{c,500 hPa}$ / $E_\mathrm{c,1000 hPa}$')
  ax2.legend()
  #--------------------------------------------------------------------------------------------------

  ax3 = plt.subplot( row, col, 3)
  ax3.set_xlabel('$d_2$ / $d_1$')

  ax3.set_ylabel('$K$')
  ax3.set_xlim(0.,1)
  ax3.set_ylim(1e-6,7e-5)
  ax3.set_yscale('log')
  ax3.tick_params(axis='both', which='major', length=8, width=2)
  ax3.tick_params(axis='both', which='minor', length=5, width=2)
  #--------------------------------------------------------------------------------------------------
  ax3.plot(tab1["d2"]/tab1["d1"],tab1["kernel"], color='black', label='$E_\mathrm{c,1000 hPa}$')
  ax3.plot(tab2["d2"]/tab2["d1"],tab2["kernel"], color='green', label='$E_\mathrm{c,750 hPa}$')
  ax3.plot(tab3["d2"]/tab3["d1"],tab3["kernel"], color='blue',  label='$E_\mathrm{c,500 hPa}$')
  ax3.legend()
  #--------------------------------------------------------------------------------------------------
  ax4 = plt.subplot( row, col, 4)
  ax4.set_xlabel('$d_2$ / $d_1$')

  ax4.set_ylabel('relative $K$ [%]')
  ax4.set_xlim(0.,1)
  ax4.set_ylim(limit[2],limit[3])
  ax4.yaxis.set_minor_locator(MultipleLocator(10))
  ax4.yaxis.set_major_locator(MultipleLocator(20))
  # ax4.set_yscale('log')
  ax4.tick_params(axis='both', which='major', length=8, width=2)
  ax4.tick_params(axis='both', which='minor', length=5, width=2)
  #--------------------------------------------------------------------------------------------------
  ax4.plot(tab2["d2"]/tab2["d1"],tab2["kernel"]/tab1["kernel"]*100, color='green', label='$K_\mathrm{c,750 hPa}$ / $K_\mathrm{c,1000 hPa}$')
  ax4.plot(tab3["d2"]/tab3["d1"],tab3["kernel"]/tab1["kernel"]*100, color='blue',  label='$K_\mathrm{c,500 hPa}$ / $K_\mathrm{c,1000 hPa}$')
  ax4.legend()
  #--------------------------------------------------------------------------------------------------
  # ax5 = plt.subplot( row, col, 5)
  # ax5.set_xlabel('$d_2$ / $d_1$')

  # ax5.set_ylabel('Re')
  # ax5.set_xlim(0.,1)
  # ax5.set_ylim(0.,2.5)
  # ax5.yaxis.set_minor_locator(MultipleLocator(.25))
  # # ax4.yaxis.set_major_locator(MultipleLocator(20))
  # # ax4.set_yscale('log')
  # ax5.tick_params(axis='both', which='major', length=8, width=2)
  # ax5.tick_params(axis='both', which='minor', length=5, width=2)
  # #--------------------------------------------------------------------------------------------------
  # ax5.plot(tab1["d2"]/tab1["d1"],tab1["Re"], color='black', label='$\mathrm{Re}_{1000 hPa}$')
  # ax5.plot(tab2["d2"]/tab2["d1"],tab2["Re"], color='green', label='$\mathrm{Re}_{750 hPa}$')
  # ax5.plot(tab3["d2"]/tab3["d1"],tab3["Re"], color='blue',  label='$\mathrm{Re}_{500 hPa}$')
  # ax5.legend()
  #--------------------------------------------------------------------------------------------------
  # ax6 = plt.subplot( row, col, 6)
  # ax6.set_xlabel('$T$ $[^{\circ}C]$')
  # ax6.set_ylabel('$\eta$')

  # ax6.set_xlim(-30,20)
  # # ax6.set_ylim(0.,2.5)
  # T = np.linspace(-30, 20, num=51)
  # eta = 1.718e-5 * (1. + T * (0.00285215366705471478 - T*6.9848661233993015133e-6)) 
  # ax6.plot(T, eta)
  # ax6.axvline(x=19, color = "red", linestyle='-.')
  # ax6.axvline(x=0,  color = "red", linestyle='-.')
  #--------------------------------------------------------------------------------------------------
  plt.tight_layout(rect=[0, 0.0, 1, 0.9])

  pp.savefig(bbox_inches = 'tight', pad_inches = 0.5)
  pp.close()




