from plotpack import *
from glob import glob

#--------------------------------------------------------------------------------------------------
save_name = 'PDFs/capacitance_vs_phi.pdf'
#--------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(20,15))
fig.subplots_adjust(wspace=0, hspace=0)
pp      = PdfPages(save_name)

font    = {'size'   : 34}
mp.rc('font', **font)

#--------------------------------------------------------------------------------------------------
ax1  = plt.subplot(1,1,1)

ax1.set_xlabel('$\phi$')
ax1.set_ylabel('$\overline{C}$')
ax1.set_xlim(0.1,10)

ax1.xaxis.set_minor_locator(MultipleLocator(.1))
ax1.tick_params(axis='both', which='major', length=8, width=2)
ax1.tick_params(axis='both', which='minor', length=5, width=2)
#--------------------------------------------------------------------------------------------------
phi = np.logspace(-1.0, 1.0, num=101)
C = np.full(phi.shape, 0.)
V = 1000 
for i in range(0,101):
  a   = ( 0.75 * V / (np.pi * phi[i]) )**(1./3.) 
  b   = a * phi[i]
  
  if phi[i] < 1.:
    eps = np.sqrt(1. - phi[i]**2)
    C[i]   = a * eps / np.arcsin(eps)
  elif phi[i] > 1:
    eps = np.sqrt( 1. - phi[i]**-2 )
    C[i]   = b * eps / np.log( (1. + eps) * phi[i] )
  else:
    C[i]   = (2. * a + b) / 3.

#--------------------------------------------------------------------------------------------------
ax1.set_xscale('log')
# ax1.set_yscale('log')
plt.plot(phi,C/C[50], color = 'black')
ax1.axvline(x=1.)
#--------------------------------------------------------------------------------------------------

plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)

plt.show()
pp.close()