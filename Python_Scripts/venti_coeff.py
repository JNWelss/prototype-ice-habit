from plotpack import *
from glob import glob
import math
import matplotlib.lines as mlines
#--------------------------------------------------------------------------------------------------

Sc = 0.63
Re_col = [.2,.5,.7,1.,2.,5.,10.,20.]
Re_hex = [1.,2.,10.,20.,35.0,60.,90.,120.]

c_col  = [2.85,2.85,3.08,3.33,4.44,6.67,10.,16.67]
c_hex  = [.2250,.1770,.1265,.1034,.0863,.0725,.0640,.0576]

d_col  = [2.,2.,2.,2.,2.,2.,2.,2.]
d_hex  = [2.,2.,2.,2.,2.,2.,2.,2.]
#--------------------------------------------------------------------------------------------------
X_col = np.zeros(shape=(8))
X_hex = np.zeros(shape=(8))

venti_col = np.zeros(shape=(8))
venti_hex = np.zeros(shape=(8))
venti_col_ice = np.zeros(shape=(8))
venti_hex_ice = np.zeros(shape=(8))
#--------------------------------------------------------------------------------------------------
pp  = PdfPages('./PDFs/venti_coeff.pdf')
#--------------------------------------------------------------------------------------------------
col, row = 1, 2
fig, ax = plt.subplots(col*row,figsize=(15*col,10*row))
ax1 = plt.subplot(row, col, 1)

ax1.set_xlim(0.,10)
ax1.set_ylim(0.7,3.5)
ax1.set_xlabel('X')
ax1.set_ylabel('$\overline{f_\mathrm{v}}$')
#--------------------------------------------------------------------------------------------------
for i in range(8):
  X_col[i]  = Sc**(1/3) * np.sqrt(Re_col[i]) 
  X_hex[i]  = Sc**(1/3) * np.sqrt(Re_hex[i]) 
  phi_col= 0.5*d_col[i] / 0.5 * c_col[i]
  phi_hex= 0.5*d_hex[i] / c_hex[i]

  # columnar
  venti_col[i] = 1.0 - 0.00668*(X_col[i]/4) + 2.39402 * (X_col[i]/4)**2 + 0.73409 * (X_col[i]/4)**3 - 0.73911 * (X_col[i]/4)**4
  # plates
  venti_hex[i] = 1.0 - 0.60420*(X_hex[i]/10) + 2.79820 * (X_hex[i]/10)**2 + 0.31933 * (X_hex[i]/10)**3 - 0.06247 * (X_hex[i]/10)**4    

  if X_col[i] < 1.0:
    venti_col_ice[i] = 1.0  + 0.14*X_col[i]**2
  else:
    venti_col_ice[i] = 0.86 + 0.28*X_col[i]

  if X_hex[i] < 1.0:
    venti_hex_ice[i] = 1.0  + 0.14*X_hex[i]**2
  else:
    venti_hex_ice[i] = 0.86 + 0.28*X_hex[i]
#--------------------------------------------------------------------------------------------------
ax1.plot(X_col,venti_col, c='black', label='col')
ax1.plot(X_hex,venti_hex, c='black', linestyle='--', label='hex')
ax1.plot(X_col,venti_col_ice, c='red', label='col')
ax1.plot(X_hex,venti_hex_ice, c='red', linestyle='--', label='hex')
#--------------------------------------------------------------------------------------------------
handles = []
handles.append(mlines.Line2D([], [], color='black', marker='None', linestyle='-', label='Ji & Wang 99'))
handles.append(mlines.Line2D([], [], color='red', marker='None', linestyle='-', label='Pruppacher 97'))
leg = plt.legend(handles=handles, loc='upper left')
ax1.add_artist(leg)

handles2 = []
handles2.append(mlines.Line2D([], [], color='black', marker='None', linestyle='-', label='columns'))
handles2.append(mlines.Line2D([], [], color='black', marker='None', linestyle='--', label='plates'))
leg = plt.legend(handles=handles2, loc='lower right')
#--------------------------------------------------------------------------------------------------
ax2 = plt.subplot(row, col, 2)

ax2.set_xlim(0.,120)
ax2.set_ylim(0.7,3.5)
ax2.set_xlabel('Re')
ax2.set_ylabel('$\overline{f_\mathrm{v}}$')
ax2.plot(Re_col,venti_col, c='black', label='col')
ax2.plot(Re_hex,venti_hex, c='black', linestyle='--', label='hex')
ax2.plot(Re_col,venti_col_ice, c='red', label='col')
ax2.plot(Re_hex,venti_hex_ice, c='red', linestyle='--', label='hex')

#--------------------------------------------------------------------------------------------------
plt.tight_layout(rect=[0, 0.0, 1, 0.9])

pp.savefig(bbox_inches = 'tight', pad_inches = 0.1)
pp.close()
# plt.show()

