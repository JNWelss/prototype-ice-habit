from plotpack import *
from glob import glob
import math
#--------------------------------------------------------------------------------------------------

eta0  = 1.718e-5
A_S   = 1.458e-6
T_S   = 110.4

T = np.linspace(253.15, 273.15, 21)
eta1 = np.zeros(shape=(21))
eta2 = np.zeros(shape=(21))


for i in range(21):
  Tc   = T[i] - 273.15

  eta1[i] = eta0 * (1.0 + Tc * (0.00285215366705471478 - Tc * 6.9848661233993015133e-6))
  eta2[i] = A_S * T[i]**(3./2.) / ( T[i] + T_S )

plt.plot(T,eta1)
plt.plot(T,eta2,c="r")
plt.show()