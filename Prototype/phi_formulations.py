from plotpack import *

def area(xi,a):
    return xi * np.pi * a**2
#---------------------------------------------------------------------------------------------------
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 22}

lines   = {'linewidth':'3',
           'markersize':'5',
           'markeredgewidth':'1.5' }

rc      = {"axes.spines.right" : False,
           "axes.spines.top" : False, }
plt.rcParams.update(rc)

mp.rc('font', **font)
mp.rc('lines', **lines)
#---------------------------------------------------------------------------------------------------
fig = plt.figure(figsize=(16,6))
colors = ['black','darkgray','silver']
#---------------------------------------------------------------------------------------------------
lamb = 4.7e-3
d = np.linspace(1e-7,1e-3,1001)

phi_1 = 1.05 - 0.655 * d * 100.
phi_1[phi_1 > 1.] = 1.
phi_2 = np.exp(-d/lamb) + (1. - np.exp(-d/lamb)) / (1. + d / lamb) 
#---------------------------------------------------------------------------------------------------
ax1  = plt.subplot(1,1,1)

ax1.set_xlabel('$d_0$ [$\mu$m]')
ax1.set_ylabel('$\phi$')

# plt.ylim(0,1)
plt.xscale('log')

ax1.set_xlim(1e-1,1e3)
ax1.set_ylim(np.min(phi_2),1.0001)

ax1.plot(d*1e6, phi_1, label='Boehm', color='black' )
ax1.plot(d*1e6, phi_2, label='Pruppacher & Klett', color='tab:blue' )

print("max. diff  "+str(np.max(phi_1/phi_2)))

plt.legend()
#---------------------------------------------------------------------------------------------------
plt.tight_layout()

plt.savefig('phi_formulations.pdf')
plt.show()

plt.close()