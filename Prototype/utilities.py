import numpy as np
import pandas as pd
import time
import os
from namelist import *
#--------------------------------------------------------------------------------------------------
# atmospheric constants
T_3   = 273.15
Rv    = 461.401
c_pa  = 1005.
grav  = 9.81
rho_i = 919.    
rho_a = 1.27
#--------------------------------------------------------------------------------------------------
# specify directory and format string for output
directory = './output/'
# create output directory for output data
if not os.path.exists(directory):
    os.makedirs(directory)
# Format for variables ['t','T','a','b','phi','m','V','vt', 'D']
form      = ('%5.1f,%3.3f,%1.8f,%1.8f,%1.8f,%1.15f,%1.18f,%1.4f,%1.8f') 
#--------------------------------------------------------------------------------------------------
#load inherent growth function
if IGF == 1:
    filename = 'curve_inherent.txt'
elif IGF == 2:
    filename = 'IGF_new_poly.txt'
elif IGF == 3:
    filename = 'IGF_fitted.txt'
tab  = pd.read_csv(filename, delimiter = "   ", names=['T','IGF'], engine='python', dtype = 'float64' )
# IGF derived from Takahashi 91 results
# T_TH = np.array([-3.7,-5.2,-8.6,-10.6,-12.2,-14.4,-16.5,-18.2,-20.1,-22.0])
# IGF_TH= np.array([0.913,3.222,1.093,1.104,0.478,0.510,0.649,0.328,0.815,0.980])

# tab=np.array([T_TH,IGF_TH]).T
# tab=tab[::-1]
#--------------------------------------------------------------------------------------------------

# calculate the atmospheric state variables
def atmo_variab(T,p):
    Tc      = T - T_3
    eta     = 1.718e-5 * ( 1.0 + Tc * (0.00285215366705471478 - Tc * 6.9848661233993015133e-6) )
    L_depo  = 2834100 - Tc * ( 290.0 + Tc * 4.0 ) 
    p_sat_i = 6.10780000e2 * np.exp( 2.18745584e1 * Tc / (T - 7.66000000e0) )
    p_sat_w = 6.10780000e2 * np.exp( 1.72693882e1 * Tc / (T - 3.58600000e1) )
    D_v     = 0.211e-4 * (T / T_3)**1.94 * 101325.0 / p
    Ka      = 418.4e-5 * ( 5.69 + 0.017 * Tc )

    return eta, L_depo, p_sat_i, p_sat_w, D_v, Ka

# calculate capacitance for a crystal with habit
def capac(a,b):
    phi = b / a
    if phi < 1.:
        eps = np.sqrt(1.0 - phi**2)
        C   = a * eps / np.arcsin(eps)
    elif phi > 1.:
        eps = np.sqrt(1.0 - phi**-2)
        C = b * eps / np.log( (1. + eps) * phi )
    else:
        C = np.cbrt(a**2 * b)

    return C

# convert aspect ratio and volume to explicit axis length and determine the max. dimension/diameter
def m_to_D(V, phi):
    a     = np.cbrt(0.75 * V / (np.pi * phi) )
    b     = a * phi
    d     = 2. * max(a,b)

    return a,b,d

def m_to_D_relation(m):
    beta_i  = 2.1
    gamma_i = 1.88
    alpha_i = 2.8 * 10**(2. * beta_i - 6.) 
    sigma_i = 2.285 * 10**(2. * gamma_i - 5.)
    d_th    = (np.pi * rho_i / (6.0 * alpha_i) )**(1./(beta_i-3.))
    m_th    = np.pi / 6. * rho_i * d_th**3

    if m < m_th:
        d    = np.cbrt(6. * m / (np.pi * rho_i))
        area = np.pi/4. * d**2 
    else:
        d    = (m/alpha_i)**(1./beta_i)
        area = sigma_i * d**(gamma_i)

    a = d * .5
    b = a

    return a,b,d,area

# fetch IGF value from lookup table
def lookup_IGF(temp):
    temp = temp - 273.15 
    if temp >= tab['T'].iloc[-1]:
        return tab['IGF'].iloc[-1]
    elif temp < tab['T'].iloc[0]:
        return tab['IGF'].iloc[0]

    for i in range(len(tab['T'])):
        if tab['T'][i] > temp:
            interpolation = (temp - tab['T'][i-1]) / (tab['T'][i] - tab['T'][i-1])
            gamma         = tab['IGF'][i-1] + interpolation * (tab['IGF'][i] - tab['IGF'][i-1])
            return gamma
            break

# saves data into output array, depends if T-loop used or not
def output(var,t,T,a,b,phi,m,V,vt,loop,temp):
    D = 2. * max(a,b)
    if loop == 1:
        var[temp,0:9] = (t, T, a, b, phi, m, V, vt, D)
        # specify output string for data files
        fname = 'T_range_'+str(int(-n*T_inc))+'-0_p'+ str(p/100)+'_dt'+str(dt)+'_t_end'+str(int(t)) + \
             '_r_start' + str(r_start)+'_mD_'+str(mD)+'_IGF_'+str(IGF)+'_depend_'+str(depend)+'_depo_'+str(depo_scheme)+'.out'
        np.savetxt(directory+fname, var, delimiter=',', fmt=form)

    if loop == 2:
        i = int(t)
        var[i,0:9] = (t, T, a, b, phi, m, V, vt, D)


# print status (for debugging)
def print_status(t,t_end,m,V,a,b,phi,dt):
    if t == 0:
        print(" Start ")
    elif t == t_end:
        print(" End ")
    else:
        print(" Timestep #" +str(int(t / dt)) )
    print("time = " +str(t)+ " m = "   +str(m)+ " V = "   +str(V)+ " rho_eff = " + str(m/V) )
    print("r_a  = " +str(a)+ " r_b = " +str(b)+ " phi = " +str(phi))  

# deposition density modification in case of branching/hollowing
def rho_depo(vt,D_v,a,b,gamma):
    if gamma < 1.0:
        # if ( (vt * a**2) > (np.pi * D_v * b) ):
        if a > 200e-6:
            rho_dep = rho_i * gamma
        else:
            rho_dep = rho_i
    else:
        rho_dep = rho_i / gamma

    return rho_dep

# Chen and Lamb deposition density modification in case of branching/hollowing
def rho_depo_CL(p_vap, p_sat_w, p_sat_i, T, gamma):
    ssi_max  = p_sat_w / p_sat_i - 1.0
    ssi      = p_vap / p_sat_i - 1.0
    Diff_C   = 2.52E-5
    LatHet_S = 2.8342E6
    Heat_C   = 2.550E-2
    Fac_ice_dd_invrhoi = 461.401 / Diff_C * T / p_sat_i
    Fac_ice_kk_invrhoi = ( 1. / T * LatHet_S / 461.401 - 1.0 ) * (LatHet_S/Heat_C) * 1. / T
    excess_vap_dens = 1000.0 * min(ssi,ssi_max) / ( Fac_ice_dd_invrhoi + Fac_ice_kk_invrhoi ) / Diff_C
    rho_dep   = 0.91 * np.exp(-3. * max([excess_vap_dens - 0.05, 0.]) / gamma)

    return rho_dep * 1000.

# calculate ventilation coefficient for Dv and Ka depending on flow characteristics
# third argument defines if phi dependency is activated
# General fit:
# prolates:  fv = fv + c1 * phi * X,        for phi > 1; c1 = 0.028
# oblates:   fv = fv + c2 / phi * X^3/2,    for phi < 1; c2 = 0.0028
# for X > 1:
# prolates: fv = fv + c1 * phi * (X-1),     for phi > 1; c1 = 0.0300
# oblates:  fv = fv + c2 / phi * (X-1)^2,   for phi < 1; c2 = 0.0045
def ventilation_phi(X, phi):

    if (phi >= 1.0):
        X_equiv = X * phi**(-1./3.)
    elif (phi < 1.0):
        X_equiv = X * phi**(1./6.)

    if (X_equiv < 1.0):
        fv = 1.0  + 0.14*X_equiv**2
    else:
        fv = 0.86 + 0.28*X_equiv

    if (depend == 1):
        c1 = 0.028
        c2 = 0.0028
        if (phi > 1.0):
            fv      = fv + c1 * phi * X_equiv
        elif (phi < 1.0):
            fv      = fv + c2 / phi * X_equiv**(3./2.) 
        # c1 = 0.030
        # c2 = 0.0045
        # if (phi > 1.0):
        #     fv      = fv + c1 * phi * (X_equiv-1)
        # elif (phi < 1.0):
        #     fv      = fv + c2 / phi * (X_equiv-1)**(2.) 

    return fv

# calculate ventilation coefficient for the crystal sides
def ventilation_f(X,axis,r_0,phi):
    if (phi >= 1.0):
        X_equiv = X * phi**(-1./3.)
    elif (phi < 1.0):
        X_equiv = X * phi**(1./6.)
    if X < 1.0:
        fv = 1.0  + 0.14*X_equiv**2 * np.sqrt(axis/r_0)
    else:
        fv = 0.86 + 0.28*X_equiv    * np.sqrt(axis/r_0)

    return fv

# terminal velocity scheme after Boehm, includes ice habit
def vterm_bohm(m, V, d, a, phi, eta):
    # determine cross-sect. area of crystal for vt-scheme
    if phi > 1.:
        c    = a * phi
        area = np.pi * a * c                    
        q = 4. / np.pi
        d = 2. * a
    else:
        #area correction for branched oblates only (Boehm '89)
        area = (m / V) / rho_i * np.pi * a**2 
        q = area / (np.pi / 4.0 * d**2)
        d = 2. * a     

    inc = 3.
    if ( (phi > 1.) & (phi <= (1. + inc) ) ):
        N_Re_cyl = N_Re_bohm(m, phi, q, eta)

        d_pro    = 2. * ( 0.75 * V / (np.pi * phi) )**(1./3.)
        N_Re_pro = N_Re_bohm(m, phi, area / (np.pi / 4. * (2.* c)**2 ), eta)
    
        weigh    = (phi - 1. ) / inc
        d        = weigh * d + (1. - weigh) * d_pro  
        N_Re     = weigh * N_Re_cyl + (1. - weigh) * N_Re_pro 

    else:
        N_Re = N_Re_bohm(m, phi, q, eta)
  
    vterm_bohm = N_Re * eta / d / rho_a

    return vterm_bohm

# terminal velocity scheme after Heymsfield
def vterm_hw10(m,d,eta):
    do_i = 8.0
    co_i = 0.35

    # modified Best number eq. on p. 2478
    p_area  = (np.pi / 4.0) * d**2.0
    Ar      = p_area * 4.0 / np.pi
    Xbest   = rho_a * 8.0 * m * grav * d / (eta**2 * np.pi * np.sqrt(Ar))

    # Re-X eq. on p. 2478
    c1      = 4.0 / ( do_i**2 * np.sqrt(co_i) )
    c2      = 0.25 * do_i**2
    bracket = np.sqrt(1.0 + c1 * np.sqrt(Xbest)) - 1.0
    Re      = c2 * bracket**2

    vt = eta * Re / (rho_a * d)

    return vt 

# Reynolds-Number following Boehm
def N_Re_bohm(m, phi, q, eta):
    X = 8.0 * m * grav * rho_a / (np.pi*(eta**2) * max(phi,1.0) * max(q**(1.0/4.0),q))

    k = min( max(0.82 + 0.18 * phi,0.85),0.37 + 0.63 / phi, 1.33 / (max( np.log(phi),0.0) + 1.19) )

    gama_big  = max( 1.0, min(1.98, 3.76 - 8.41 * phi + 9.18 * phi**2 - 3.53 * phi**3) )

    C_DP      = max( 0.292 * k * gama_big, 0.492 - 0.2 / np.sqrt(phi) )
    C_DP      = max( 1.0, q * (1.46 * q - 0.46) ) * C_DP

    X_0       = 2.8e6
    C_DP_prim = C_DP * ( 1.0 + 1.6 * (X / X_0)**2) / (1.0 + (X / X_0)**2)


    beta  = np.sqrt(1.0 + C_DP_prim / 6.0 / k * np.sqrt(X / C_DP_prim)) - 1
    N_Re0 = 6.0 * k / C_DP_prim * beta**2

    #low Reynolds number
    C_DO       = 4.5 * k**2 * max(phi,1.0)
    gama_small = (C_DO - C_DP) / 4.0 / C_DP

    N_Re       = N_Re0 * (1.0 + (2.0 * beta * np.exp(-beta * gama_small)) / 
                 ( (2.0 + beta) * (1.0 + beta)) )
    return N_Re

def vterm_hw10_mD_relation(m,d,eta,p_area):
    do_i = 8.0
    co_i = 0.35

    # modified Best number eq. on p. 2478
    Ar      = p_area * 4.0 / np.pi
    Xbest   = rho_a * 8.0 * m * grav * d / (eta**2 * np.pi * np.sqrt(Ar))

    # Re-X eq. on p. 2478
    c1      = 4.0 / ( do_i**2 * np.sqrt(co_i) )
    c2      = 0.25 * do_i**2
    bracket = np.sqrt(1.0 + c1 * np.sqrt(Xbest)) - 1.0
    Re      = c2 * bracket**2

    vt = eta * Re / (rho_a * d)

    return vt 
