#--------------------------------------------------------------------------------------------------
trange = [248, 251, 258, 261, 263, 268]
# experiment namelist: pressure, initial radius, temperature
p        = 101000.00     # [Pa]     TF'88 860 hPa, TF'91 1010 hPa    
r_start  = 2e-6          # [mikro m]
T        = trange[2]+.15        # [K] 260.95, 262.65 
# time step, end time
dt       = 1.0           # [s]
t_end    = 600           # [s]
# term. velocity scheme
v_scheme = 2             # 1: Heymsfield, 2: Boehm
# phi dependency of ventilation
depend      = 0          # 0: disabled, 1:enabled
depo_scheme = 1          # 1: J&H 15, 2: CL'94

# choose if you want automated postprocessing and/or a loop over temperature range 'n'
postpro  = 0
T_loop   = 1             # 1: loop, 2: specific temperature
mD       = 1             # 1: habit-predict, 2: m-D-relation, 3: spherical particle
T_inc    = 0.25
dt_out   = [180.]
n        = 120            # T-range size to use, always starts from 273.15K
IGF      = 1              # IGF: 1: original Chen&Lamb, 2: new formulation
#--------------------------------------------------------------------------------------------------