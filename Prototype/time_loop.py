from utilities import *                                         # import functions and constants
from namelist  import *

# actual time loop of model
def prog_loop(T,temp,var):
    r      = r_start
    phi    = 1.0        
    vt     = 0.0    
    gamma  = lookup_IGF(T)                                       # get IGF since T const.

    # set up ice crystal mass, volume, axis lengths, and max dimension
    V      = 4./3. * np.pi * r**3
    m      = rho_i * V
    if mD == 1:
        a,b,d  = m_to_D(V,phi)
    if mD == 2:
        a,b,d,area  = m_to_D_relation(m)
    if mD == 3:
        a,b   = r, r
        d     = 2. * r
        area  = np.pi/4. * d**2 
        
    
    # atmospheric state
    eta, L_depo, p_sat_i, p_sat_w, D_v, Ka = atmo_variab(T,p)                  
    p_vap  = p_sat_w                                            # supersat. at water saturation
    # cubic root of Prandtl and Schmidt number (constant if T & p constant)
    cubrPr = np.cbrt( eta * c_pa / Ka )
    cubrSc = np.cbrt( eta / (rho_a * D_v) )

    t     = 0.0
    if (T_loop == 2):
        output(var,t,T,a,b,phi,m,V,vt,T_loop,temp)

    while t < t_end:
        # terminal velocity scheme, specified in namelist
        if v_scheme == 1:
            if mD == 1: 
                vt = vterm_hw10(m,    d,         eta)
            else:
                vt = vterm_hw10_mD_relation(m,    d,         eta, area)
        else:
            vt = vterm_bohm(m, V, d, a, phi, eta)    
        # update square root of Reynolds number, phi dependent vent. coeff. needs d_equiv
        # d = cbrt( 6 * V / pi)
        sqrtRe = np.sqrt( rho_a * vt * np.cbrt( 6. / np.pi * V  ) / eta )
# 
        # correct D_v & Ka for ventilation effects (multiplied by f_bar)
        D_v_f  = D_v * ventilation_phi(cubrSc * sqrtRe, b/a)
        Ka_f   = Ka  * ventilation_phi(cubrPr * sqrtRe, b/a)

        # vent. correction for gamma with f = fb/fa
        C       = capac(a,b)
        fb      = ventilation_f(cubrSc * sqrtRe, b, np.cbrt(a**2 * b), b/a )
        fa      = ventilation_f(cubrSc * sqrtRe, a, np.cbrt(a**2 * b), b/a ) 
        gamma_f = fb / fa * gamma

        Hi      = (L_depo / (Rv * T) - 1.0) * (L_depo * D_v_f * p_sat_i) / (Ka_f * Rv * T**2)

        dt_m    = 4.0 * np.pi * C * D_v_f * (p_vap - p_sat_i) / (Rv * T * (1 + Hi))
        # use rho_depo to consider branching/hollowing
        if mD ==1:
            if depo_scheme == 1:
                dt_V    = 1.0 / rho_depo(vt,D_v_f,a,b,gamma) * dt_m
            else:
                dt_V    = 1.0 / rho_depo_CL(p_vap, p_sat_w, p_sat_i, T, gamma) * dt_m
        else:
            dt_V = dt_m / rho_i

        # update V & predict phi
        V_new   = max (V + dt * dt_V, m / rho_i )
        if mD == 1:
            if IGF == 2:
                phi = phi * ( V_new / V )**( (gamma_f - 1.0) / (gamma_f + 2.0))
            else:
                if (2. * max(a,b) ) >= 1e-5:
                    # if phi >= 30.:
                        # gamma_f = 1.0
                    phi = phi * ( V_new / V )**( (gamma_f - 1.0) / (gamma_f + 2.0))
                else:
                        phi = 1.
        else:
            phi     = 1.
      
        # update time level
        V = V_new
        m = m + dt * dt_m
        t = t + dt
        #diagnose a & b
        if mD == 1:
            a,b,d  = m_to_D(V,phi)
        if mD == 2:
            a,b,d,area  = m_to_D_relation(m)
        if mD == 3:
            a = np.cbrt(0.75*V / np.pi)
            b = a
            d = 2. * a
            area  = np.pi/4. * d**2 
        if (T_loop == 2):
            output(var,t,T,a,b,phi,m,V,vt,T_loop,temp)    
    output(var,t,T,a,b,phi,m,V,vt,T_loop,temp)
      
    return var
