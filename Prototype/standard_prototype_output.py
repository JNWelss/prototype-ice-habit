from plotpack  import *
from utilities import *

#--------------------------------------------------------------------------------------------------
directory = './output/'
names     = ['t','T','a','b','phi','m','V','vt']
#--------------------------------------------------------------------------------------------------
fname = 'T_range_-30-0_p1100.0_dt1.0_t_end600_r_start4e-06_mD_'
#--------------------------------------------------------------------------------------------------

pp       = PdfPages('standard_prototype_output.pdf')

tab      = pd.read_csv(directory+fname+'1.out',              delimiter = ",", names=names)
tab2     = pd.read_csv(directory+fname+'2.out',              delimiter = ",", names=names)
tab3     = pd.read_csv(directory+fname+'3.out',              delimiter = ",", names=names)
taka_a   = pd.read_csv('./paper_dat/Takahashi91_a_axis.dat', delimiter = ",", names=['T','a'])
taka_b   = pd.read_csv('./paper_dat/Takahashi91_b_axis.dat', delimiter = ",", names=['T','b'])
taka_m   = pd.read_csv('./paper_dat/Takahashi91_m.dat',      delimiter = ",", names=['T','m'])
taka_phi = pd.read_csv('./paper_dat/Takahashi91_phi.dat',    delimiter = ",", names=['T','phi'])

df_wt    = pd.read_csv('./paper_dat/JH_fig2_WT.dat',         delimiter = ",", names=['T','rho'])
df_jh    = pd.read_csv('./paper_dat/JH_fig2_JH.dat',         delimiter = ",", names=['T','rho'])
df_cl    = pd.read_csv('./paper_dat/JH_fig2_CL.dat',         delimiter = ",", names=['T','rho'])

dfs     = [taka_a, taka_b, taka_m, taka_phi]
resu    = reduce(lambda left,right: pd.merge(left,right,on='T'), dfs)       # sort data for T
#---------------------------------------------------
fig     = plt.figure(figsize=(18,12))
nrow    = 2
ncol    = 2
labels  = ['prototype','Fukuta 1969','Jensen & Harrington 2015','Chen & Lamb 1994']

font    = {'family'  : 'sans-serif',
            'weight' : 'normal',
            'size'   : 22}

lines   = {'linewidth'      :'3',
           'markersize'     :'5',
           'markeredgewidth':'1.5' }

rc      = {"axes.spines.right" : False,
           "axes.spines.top"   : False, }
plt.rcParams.update(rc)

mp.rc('font', **font)
mp.rc('lines', **lines)
#---------------------------------------------------
ax      = plt.subplot(nrow, ncol, 1)
plt.xlabel('$T$ [K]')
plt.ylabel('axis lengths $a,\,c$ [$\mu$m]')
# plt.title('axis lengths compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

plt.plot(tab["T"],tab["a"]/1e-6, label="a", color='black')
plt.plot(tab["T"],tab["b"]/1e-6, label="c", color='darkgrey')

plt.plot(resu["T"]+273.15,resu["a"], marker='s', linestyle='None', mfc='none', 
         color='tab:blue', label='_nolegend_')
plt.plot(resu["T"]+273.15,resu["b"], marker='o', linestyle='None', mfc='none', 
         color='tab:blue', label='_nolegend_')

plt.yscale('log')
plt.xlim(min( tab["T"]), max(tab["T"]))
plt.ylim(1, 5000)
plt.legend(fontsize = 12)
#---------------------------------------------------
ax      = plt.subplot(nrow, ncol, 2)
plt.xlabel('$T$ [K]')
plt.ylabel('$\u03A6$')
# plt.title('aspect ratio compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

plt.plot(tab["T"],tab["phi"], color='black')
plt.plot(resu["T"]+273.15,resu["phi"], marker='s', linestyle='None', mfc='none', 
         color='tab:blue', label='_nolegend')

plt.yscale('log')
plt.xlim(min(tab["T"]),max(tab["T"]))
plt.axhline(y=1,linestyle='--', color='black', linewidth=0.5)
#---------------------------------------------------
ax      = plt.subplot(nrow, ncol, 3)
plt.xlabel('$T$ [K]')
plt.ylabel('$m$ [$\mu$g]')
# plt.title('crystal mass compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

plt.plot(tab["T"],tab["m"]/1e-9, color='black')
plt.plot(tab2["T"],tab2["m"]/1e-9, color='grey')
plt.plot(tab3["T"],tab3["m"]/1e-9, color='red')
plt.plot(resu["T"]+273.15,resu["m"], marker='s', linestyle='None', mfc='none', 
         color='tab:blue', label='_nolegend')

plt.yscale('log')
plt.xlim(min(tab["T"]),max(tab["T"]))
plt.ylim(8e-2,3e1)
#---------------------------------------------------
ax      = plt.subplot(nrow, ncol, 4)
plt.xlabel('$T$ [K]')
plt.ylabel(r'$\rho_\mathrm{eff.}$ [kg m$^{-3}$]')
# plt.title('eff. density after 10 min of vapor growth', fontsize=18)

plt.plot(tab["T"],tab["m"]/tab["V"],                     color='black',    label=labels[0])
plt.plot(df_wt["T"]+273.15,df_wt["rho"], linestyle='--', color='crimson',  label=labels[1])
plt.plot(df_jh["T"]+273.15,df_jh["rho"], linestyle='--', color='darkgrey', label=labels[2])
plt.plot(df_cl["T"]+273.15,df_cl["rho"], linestyle='--', color='tab:blue', label=labels[3])

plt.xlim(min(tab["T"]),max(tab["T"]))
plt.ylim(0.,1000.0)
plt.legend(fontsize = 12)
#---------------------------------------------------
plt.tight_layout()
pp.savefig()
pp.close()