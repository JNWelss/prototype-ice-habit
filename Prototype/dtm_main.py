from time_loop   import *
from postprocess import *
from namelist    import *   
import time
#--------------------------------------------------------------------------------------------------
start = time.process_time()

# Check if loop over temperature rang or just single temperature
if T_loop == 1:
    var1  = np.zeros( (n+1,9) )
    print('T range: ' +str(T_3) + ' - ' + str(T_3-n*T_inc))

    for temp in range(0, n+1, 1):
        T = 273.15 - temp * T_inc
        print('T = '+str(T) )

        #### actual time loop
        prog_loop(T,temp,var1)

    end   = time.process_time()
    fname = 'T_range_'+str(int(-n*T_inc))+'-0_p'+ str(p/100)+'_dt'+str(dt)+'_t_end'+str(int(t_end)) + \
             '_r_start' + str(r_start)+'_mD_'+str(mD)+'_IGF_'+str(IGF)+'depend_'+str(depend)+'_depo_'+str(depo_scheme)+'.out'

    if postpro == 1:
        post_T_range(fname)
else:  
    for j in range(243,274,1):
        Te = j + .15 
        var2  = np.zeros( (t_end+1,9) )
        #### actual time loop
        prog_loop(Te,0,var2)

        fname = 'T'+str(Te)+'_p'+str(p/100)+'_dt'+str(dt) +'_r_start' + str(r_start)+ \
             '_depo_scheme_'+str(depo_scheme)+'_IGF_'+str(IGF)+'_depend_'+str(depend)+'_depo_'+str(depo_scheme)+'.out'
        np.savetxt(directory+fname, var2, delimiter=',', fmt=form, header="T = "+str(Te))
        end   = time.process_time()

        if postpro == 1:
            log_time(fname,'t','m','vt','phi',r_start,Te)


print ("Model: time elapsed = " + str(end - start) + " s" )
