import os
import numpy as np
import pandas as pd
import matplotlib as mp
mp.use("TkAgg")                 #needed to make .show option work when wrong backend is used
import matplotlib.pyplot as plt
from   matplotlib.backends.backend_pdf import PdfPages
from   functools import reduce
plt.style.use('seaborn-ticks')
from namelist import depo_scheme

#--------------------------------------------------------------------------------------------------
directory = './output/'
names     = ['t','T','a','b','phi','m','V','vt']
#--------------------------------------------------------------------------------------------------

# post-processing scripts
# simple 1D plots
def simpleplot(filename,x,y):
    tab = pd.read_csv(directory+filename, delimiter = ",", names=names, header = 0)
    plt.xlabel(x)
    plt.ylabel(y)
    plt.title(filename)

    plt.plot(tab[x],tab[y])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.tight_layout()

    plt.savefig(filename+'_1D_'+str(x)+'_'+str(y)+'.pdf')
    plt.close()

def multiplot(filename,x,y,z):
    tab = pd.read_csv(directory+filename, delimiter = ",", names=names, header = 0)
    plt.xlabel('t [s]')
    plt.ylabel(y+' , '+z)
    plt.title(filename)

    plt.plot(tab[x],tab[y], label=y, color='black')
    plt.plot(tab[x],tab[z], label=z, color='darkgrey')
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    # plt.show()
    plt.tight_layout()

    plt.savefig(filename+'_tseries_'+str(y)+'_'+str(z)+'.pdf')
    plt.close()

# creates default T-range plots
def post_T_range(fname):
    post_dir = './T_range/'
    if not os.path.exists(post_dir):
        os.makedirs(post_dir)

    pp       = PdfPages(post_dir+fname+'_post.pdf')

    tab      = pd.read_csv(directory+fname,                      delimiter = ",", names=names)
    taka_a   = pd.read_csv('./paper_dat/Takahashi91_a_axis.dat', delimiter = ",", names=['T','a'])
    taka_b   = pd.read_csv('./paper_dat/Takahashi91_b_axis.dat', delimiter = ",", names=['T','b'])
    taka_m   = pd.read_csv('./paper_dat/Takahashi91_m.dat',      delimiter = ",", names=['T','m'])
    taka_phi = pd.read_csv('./paper_dat/Takahashi91_phi.dat',    delimiter = ",", names=['T','phi'])

    df_wt    = pd.read_csv('./paper_dat/JH_fig2_WT.dat',         delimiter = ",", names=['T','rho'])
    df_jh    = pd.read_csv('./paper_dat/JH_fig2_JH.dat',         delimiter = ",", names=['T','rho'])
    df_cl    = pd.read_csv('./paper_dat/JH_fig2_CL.dat',         delimiter = ",", names=['T','rho'])

    dfs     = [taka_a, taka_b, taka_m, taka_phi]
    resu    = reduce(lambda left,right: pd.merge(left,right,on='T'), dfs)       # sort data for T
#---------------------------------------------------
    fig     = plt.figure(figsize=(18,12))
    nrow    = 2
    ncol    = 2
    labels  = ['prototype','Fukuta 1969','Jensen & Harrington 2015','Chen & Lamb 1994']

    font    = {'family'  : 'sans-serif',
                'weight' : 'normal',
                'size'   : 22}

    lines   = {'linewidth'      :'3',
               'markersize'     :'5',
               'markeredgewidth':'1.5' }

    rc      = {"axes.spines.right" : False,
               "axes.spines.top"   : False, }
    plt.rcParams.update(rc)

    mp.rc('font', **font)
    mp.rc('lines', **lines)
#---------------------------------------------------
    ax      = plt.subplot(nrow, ncol, 1)
    plt.xlabel('$T$ [K]')
    plt.ylabel('axis lengths $a,\,c$ [$\mu$m]')
    # plt.title('axis lengths compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

    plt.plot(tab["T"],tab["a"]/1e-6, label="a", color='black')
    plt.plot(tab["T"],tab["b"]/1e-6, label="c", color='darkgrey')

    plt.plot(resu["T"]+273.15,resu["a"], marker='s', linestyle='None', mfc='none', 
             color='tab:blue', label='_nolegend_')
    plt.plot(resu["T"]+273.15,resu["b"], marker='o', linestyle='None', mfc='none', 
             color='tab:blue', label='_nolegend_')

    plt.yscale('log')
    plt.xlim(min( tab["T"]), max(tab["T"]))
    plt.ylim(1, 5000)
    plt.legend(fontsize = 12)
#---------------------------------------------------
    ax      = plt.subplot(nrow, ncol, 2)
    plt.xlabel('$T$ [K]')
    plt.ylabel('$\u03A6$')
    # plt.title('aspect ratio compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

    plt.plot(tab["T"],tab["phi"], color='black')
    plt.plot(resu["T"]+273.15,resu["phi"], marker='s', linestyle='None', mfc='none', 
             color='tab:blue', label='_nolegend')

    plt.yscale('log')
    plt.xlim(min(tab["T"]),max(tab["T"]))
    plt.axhline(y=1,linestyle='--', color='black', linewidth=0.5)
#---------------------------------------------------
    ax      = plt.subplot(nrow, ncol, 3)
    plt.xlabel('$T$ [K]')
    plt.ylabel('$m$ [$\mu$g]')
    # plt.title('crystal mass compared to measurements of Takahashi \'91 (10 min)', fontsize=18)

    plt.plot(tab["T"],tab["m"]/1e-9, color='black')
    plt.plot(resu["T"]+273.15,resu["m"], marker='s', linestyle='None', mfc='none', 
             color='tab:blue', label='_nolegend')

    plt.yscale('log')
    plt.xlim(min(tab["T"]),max(tab["T"]))
    plt.ylim(8e-2,3e1)
#---------------------------------------------------
    ax      = plt.subplot(nrow, ncol, 4)
    plt.xlabel('$T$ [K]')
    plt.ylabel(r'$\rho_\mathrm{eff.}$ [kg m$^{-3}$]')
    # plt.title('eff. density after 10 min of vapor growth', fontsize=18)

    plt.plot(tab["T"],tab["m"]/tab["V"],                     color='black',    label=labels[0])
    plt.plot(df_wt["T"]+273.15,df_wt["rho"], linestyle='--', color='crimson',  label=labels[1])
    plt.plot(df_jh["T"]+273.15,df_jh["rho"], linestyle='--', color='darkgrey', label=labels[2])
    plt.plot(df_cl["T"]+273.15,df_cl["rho"], linestyle='--', color='tab:blue', label=labels[3])

    plt.xlim(min(tab["T"]),max(tab["T"]))
    plt.ylim(0.,1000.0)
    plt.legend(fontsize = 12)
#---------------------------------------------------
    plt.tight_layout()
    pp.savefig()
    pp.close()
    print('created file: ' +post_dir+fname+'_post.pdf')

# creates default time series plots
def log_time(filename,x,y1,y2,y3,r_start,T):   
    tab = pd.read_csv(directory+filename, delimiter = ",", names=names, header = 0)
    if T == 262.65:
        sets = '2'
    elif T == 260.95:
        sets = '3'
    else:
        return
    df  = pd.read_csv('./paper_dat/JH_Fig1'+sets+'a.dat', delimiter = ",", names=['t','curve'])
    df2 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'b.dat', delimiter = ",", names=['t','curve'])
    df3 = pd.read_csv('./paper_dat/JH_Fig1'+sets+'c.dat', delimiter = ",", names=['t','curve'])
#---------------------------------------------------
    fig    = plt.figure(figsize=(14,12))
    labels = ['prototype', 'Jensen & Harrington 2015']

    font   = {'family'   : 'sans-serif',
                'weight' : 'normal',
                'size'   : 18}

    lines  = {'linewidth'       :'3',
               'markersize'     :'5',
               'markeredgewidth':'1.5' }

    rc     = {"axes.spines.right" : False,
              "axes.spines.top"   : False, }
    plt.rcParams.update(rc)
    
    mp.rc('font', **font)
    mp.rc('lines', **lines)
    plt.suptitle('r_start='+str(r_start*1e6)+' $\mu$m')
#---------------------------------------------------
    ax  = plt.subplot(131)
    plt.xlabel('$t$ [min]')
    plt.ylabel('$m$ [g]')

    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(1, 30)
    plt.ylim(1e-8, 2e-3)

    plt.plot(tab[x]/60,tab[y1]*1000, color='forestgreen', label=labels[0])
    plt.plot(df['t'],df['curve'],    color='black',       label=labels[1])
    ax.set_xticks([1,5,10,30])
    ax.set_xticklabels(['1','5','10','30'])
    ax.minorticks_off()
    plt.legend(fontsize = 12)
#---------------------------------------------------
    ax  = plt.subplot(132)
    plt.xlabel('$t$ [min]')
    plt.ylabel('$v$ [cm s'r'$^{-1}$]')

    plt.xlim(0, 30)
    plt.ylim(0, 140)

    plt.plot(tab[x]/60,tab[y2]*100, color='forestgreen', label=labels[0])
    plt.plot(df2['t'],df2['curve'], color='black',       label=labels[1])
    plt.xticks(np.arange(0, 31, 5.0))

#---------------------------------------------------
    ax  = plt.subplot(133)
    plt.xlabel('$t$ [min]')
    plt.ylabel('$\u03A6$') #phi

    plt.xlim(0, 30)
    plt.ylim(0, 1)

    plt.plot(tab[x]/60,tab[y3],     color='forestgreen', label=labels[0])
    plt.plot(df3['t'],df3['curve'], color='black',       label=labels[1])
    plt.xticks(np.arange(0, 31, 5.0))
#---------------------------------------------------

    plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.savefig('./SeperateTempPlots/'+filename+'_depo_scheme_'+str(depo_scheme)+'_log_tseries_'+str(y1)+'_'+str(y2)+'_'+str(y3)+'.pdf')
    plt.close()
    print('created file: ' + filename+'_depo_scheme_'+str(depo_scheme)+'_log_tseries_'+str(y1)+'_'+str(y2)+'_'+str(y3)+'.pdf')