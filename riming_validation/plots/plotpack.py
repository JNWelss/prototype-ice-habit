import os
import numpy as np
import pandas as pd
import matplotlib as mp
mp.use("TkAgg")                 #needed to make .show option work when wrong backend is used
import matplotlib.pyplot as plt
from   matplotlib.pyplot import cm
from   matplotlib.backends.backend_pdf import PdfPages
from   functools import reduce
plt.style.use('seaborn-ticks')
#--------------------------------------------------------------------------------------------------
fsize   = 22
font    = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 22}

lines   = {'linewidth':'3',
           'markersize':'5',
           'markeredgewidth':'1.5' }

rc      = {"axes.spines.right" : False,
           "axes.spines.top" : False, }
plt.rcParams.update(rc)

mp.rc('font', **font)
mp.rc('lines', **lines)