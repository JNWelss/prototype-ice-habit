from plotpack import *
from glob import glob
#--------------------------------------------------------------------------------------------------
directory = '~/cleanBoehm/mcsnow/experiments/'
var       = ['d1','d2','ce_boehm','ce_bulk','ce_spheres','rho_rime','vel_rel','St','N_Re',
             'N_ReBig','ce_BG','ce_Robin','ce_Slinn','ce_Holger']
vt        = [3]
no        = 7
dir_n     = ['1']
log       = 0
#--------------------------------------------------------------------------------------------------
if log == 0:
    ystyle ='log'
    ymin   = 0.01
else:
    ystyle ='linear'
    ymin   = 0
#--------------------------------------------------------------------------------------------------
# Read paper data
df  = []
df2 = []
files = []
filex = []
for i in range(1,no+1):
    file  = '~/DWD/Boehm_drops/Boehm_data_drops/c'+str(i)+'.dat'    # B92P2 Data
    file2 = '~/DWD/Boehm_drops/Boehm_data_drops/h'+str(i)+'.dat'    # Hall Data
    files.append(file)
    filex.append(file2)

df.append( pd.DataFrame([pd.read_csv(f, names=['r','Ec'], delimiter = ",") for f in files]) )
df2.append(pd.DataFrame([pd.read_csv(f, names=['r','Ec'], delimiter = ",") for f in filex]) )
curves  = pd.concat(df, keys=dir_n)
curves2 = pd.concat(df2, keys=dir_n)
# print(curves[0][0]['r'])
# exit()
#--------------------------------------------------------------------------------------------------
# Read model data
for v in vt:
    # print(v)
    frames    = []
    os.system('csplit '+directory+'check_c2_v'+str(v)+'/colleff_riming_drops_spectrum.dat \
                   201 {'+str(no-1)+'} -f split. >/dev/null')
    os.system('sed "s/^[ \t]*//" -i split.*')

    filenames = ['split.00']
    for n in range(1,no):
        name = 'split.0'+str(n)
        os.system('sed -i 1d '+ name )
        filenames.append(name)

    frames.append(pd.DataFrame([pd.read_csv(f, names=var, delimiter = " ") for f in filenames]) )
    #--------------------------------------------------------------------------------------------------
    result = pd.concat(frames, keys=dir_n)
    #--------------------------------------------------------------------------------------------------
    row, col = 1, 3
    c        = ['black','darkgrey','rosybrown','#bae4bc','#7bccc4','#43a2ca','#0868ac', 'blue']
    labels   = ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)']
    #--------------------------------------------------------------------------------------------------
    fig, axes = plt.subplots(figsize=(20*col,20*row), nrows = 1, ncols = 1)#, sharey = True)
    fig.subplots_adjust(wspace=0.03, hspace=0)
    pp        = PdfPages('coll_efficiency_drop_vali_v'+str(v)+'_'+ystyle+'.pdf')
    lines     = {'linewidth':'2.5'}

    mp.rc('lines', **lines)
#--------------------------------------------------------------------------------------------------
    axes.tick_params(labelcolor=(1.,1.,1., 0.0), top='off', bottom='off', left='off', right='off')
    axes.frameon = False
#--------------------------------------------------------------------------------------------------
    # subplots
    for k in range(0,len(dir_n)):
        ax1 = fig.add_subplot(row,col,1)
        ax1.set_title('Boehm',        fontsize=fsize-2)
        ax1.set_xlabel('drop radius $r$ [ym]')
        ax1.set_xlim(0,100)
        ax1.set_ylim(ymin,5)
        ax1.set_yscale(ystyle)
        ax1.set_xticks(np.arange(0, 110, 10.0))

        ax2 = fig.add_subplot(row,col,2)
        ax2.set_title('Ratio Model / Paper',        fontsize=fsize-2)
        ax2.set_xlabel('drop radius $r$ [ym]')
        ax2.set_xlim(0,100)
        # ax2.set_ylim(ymin,5)
        ax2.set_yscale(ystyle)
        ax2.set_xticks(np.arange(0, 110, 10.0))

        ax3 = fig.add_subplot(row,col,3)
        ax3.set_title('Diff Model - Paper',        fontsize=fsize-2)
        ax3.set_xlabel('drop radius $r$ [ym]')
        ax3.set_xlim(0,100)
        ax3.set_ylim(-4,4)
        ax3.set_yscale('linear')
        ax3.set_xticks(np.arange(0, 110, 10.0))

        # plt.setp(ax1.get_yticklabels(), visible=False)

        for i in range(no):
            l1 = ax1.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_boehm'], 
                     color=c[i],label=labels[i]+', $N_\mathrm{Re}$'+str(round(result.loc[dir_n[0]].loc[i][0]['N_ReBig'][0], 2) ) )
            l2 = ax1.plot(curves[0][i]['r'],  curves[0][i]['Ec'],   color=c[i], linestyle='--', label='_nolegend_')
            l3 = ax1.plot(curves2[0][i]['r'], curves2[0][i]['Ec'],  color=c[i], linestyle='dotted',  label='_nolegend_')
            l4 = ax2.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_boehm'] / curves[0][i]['Ec'], 
                     color=c[i], label='_nolegend_')
            l5 = ax3.plot(result.loc[dir_n[k]].loc[i][0]['d2']*5e5,result.loc[dir_n[k]].loc[i][0]['ce_boehm'] - curves[0][i]['Ec'], 
                     color=c[i], label='_nolegend_')

        leg1 = ax1.legend(fontsize = fsize-3)
#--------------------------------------------------------------------------------------------------
# additional legend
    from matplotlib.lines import Line2D
    custom_lines = [Line2D([0], [0], color='black', lw=4, linestyle='solid'),
                    Line2D([0], [0], color='black', lw=4, linestyle='--'),
                    Line2D([0], [0], color='black', lw=4, linestyle='dotted')]

    leg2 = ax1.legend(custom_lines, ['Model', 'B92', 'Hall'],loc=4)
    ax1.add_artist(leg1)

#--------------------------------------------------------------------------------------------------
    plt.tight_layout(rect=[0, 0.0, 1, 0.95])
    pp.savefig()
    # plt.show()
    pp.close()
    os.system('rm split*')